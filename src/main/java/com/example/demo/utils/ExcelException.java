/*
 *版权所有:深圳中讯智能信息技术有限公司
 *日        期:2017年10月11日
 * */
package com.example.demo.utils;

/**
 * 类功能描述:
 * 
 * @date 2017年10月11日
 * @version 1.0
 */
public class ExcelException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExcelException() {
		// TODO Auto-generated constructor stub
	}

	public ExcelException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ExcelException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public ExcelException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
