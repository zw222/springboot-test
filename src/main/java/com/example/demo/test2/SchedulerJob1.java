package com.example.demo.test2;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

public class SchedulerJob1 implements Job {
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        System.err.println("这是第一个任务"+new Date());
        String string = jobExecutionContext.getJobDetail().getDescription();
        System.out.println(string);
        JobKey key = jobExecutionContext.getJobDetail().getKey();
        System.out.println(key);
         String string2 = jobExecutionContext.getJobDetail().toString();
         System.out.println(string2);

    }
}


