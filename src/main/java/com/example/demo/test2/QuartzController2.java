package com.example.demo.test2;

import javax.annotation.Resource;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class QuartzController2 {
    //@Resource(name = "multitaskScheduler")
    @Autowired
    private Scheduler scheduler;

    @ResponseBody
    @RequestMapping("task1")
    public Object task1() throws SchedulerException {
        //配置定时任务对应的Job，这里执行的是ScheduledJob类中定时的方法
        JobDetail jobDetail = JobBuilder.newJob(SchedulerJob1.class).withIdentity("job1", "group1").withDescription("miaoshu").build();
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0/3 * * * * ?");
        // 每3s执行一次
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity("trigger1", "group1").withSchedule(scheduleBuilder).build();
        scheduler.scheduleJob(jobDetail, cronTrigger);

        return "任务1";
    }

    @ResponseBody
    @RequestMapping("task2")
    public Object task2() throws SchedulerException {
        //配置定时任务对应的Job，这里执行的是ScheduledJob类中定时的方法
        JobDetail jobDetail = JobBuilder.newJob(SchedulerJob2.class).withIdentity("job2", "group1").build();
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0/6 * * * * ?");
        // 每3s执行一次
        CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity("trigger2", "group1").withSchedule(scheduleBuilder).build();
        scheduler.scheduleJob(jobDetail, cronTrigger);

        return "任务2";
    }
    @ResponseBody
    @RequestMapping("task3")
    public Object task3() throws SchedulerException {
    	//配置定时任务对应的Job，这里执行的是ScheduledJob类中定时的方法
    	JobDetail jobDetail = JobBuilder.newJob(SchedulerJob2.class).withIdentity("job3", "group1").build();
    	CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule("0/1 * * * * ?");
    	// 每3s执行一次
    	CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity("trigger3", "group1").withSchedule(scheduleBuilder).build();
    	scheduler.scheduleJob(jobDetail, cronTrigger); 
    	return "任务3";
    }
}
