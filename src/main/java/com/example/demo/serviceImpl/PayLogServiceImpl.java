package com.example.demo.serviceImpl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.slaver.WdfPayLogerMapper;
import com.example.demo.pojo.WdfPayLoger;
import com.example.demo.service.PayLogService;
import com.example.demo.utils.JsonResult;
import com.example.demo.utils.ResultConst;
/**
 * 支付日志
 * @author Hasee
 *
 */
@Service
public class PayLogServiceImpl implements PayLogService {

	/**
	 * 保存日志
	 */
	@Autowired
	private WdfPayLogerMapper wdfPayLogerMapper;
	@Override
	public  synchronized JsonResult insert(Map<String,String> map) {
		int insert=0;
		try {
			WdfPayLoger loger = new WdfPayLoger();
			loger.setCreateTime(new Date());
			loger.setOrderNumber(map.get("out_trade_no"));
			loger.setUserName(map.get("userName"));
			loger.setWxMoney(new Integer(map.get("cash_fee")));
			loger.setState((byte)2);
			insert = wdfPayLogerMapper.insert(loger);
			if(insert==0) {
				return new JsonResult(ResultConst.FALSE_STATE, false, "系统错误");
			}
			return new JsonResult(ResultConst.SUCCESS_STATE, false, "保存成功");
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonResult(ResultConst.FALSE_STATE, false, "系统错误");
		}
	
	}
	@Override
	public JsonResult updateState(WdfPayLoger payLoger) {
		synchronized (this) {
			try {
				int num = wdfPayLogerMapper.updateByPrimaryOrderNum(payLoger);
				if(num==0) {
					return new JsonResult(ResultConst.FALSE_STATE, false, "系统错误");
				}
			} catch (Exception e) {
				e.printStackTrace();
				return new JsonResult(ResultConst.FALSE_STATE, false, "系统错误");
			}
		}
		
		return new JsonResult(ResultConst.SUCCESS_STATE, true, "修改成功");
	}

}
