package com.example.demo.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.master.WdfGunwanLogerMapper;
import com.example.demo.pojo.WdfGunwanLoger;
import com.example.demo.pojo.WdfGunwanLogerExample;
import com.example.demo.pojo.WdfGunwanLogerExample.Criteria;
import com.example.demo.service.GunwanLogerService;
import com.example.demo.utils.UUIDUtils;
import com.github.pagehelper.PageHelper;
@Service
@Transactional
public class GunwanLogerServiceImpl implements GunwanLogerService {
	
	@Autowired
	private  WdfGunwanLogerMapper  wdfGunwanLogerMapper;
	/**
	 * 保存官网日志
	 * @param record
	 * @return
	 */
	@Override
	public Integer insert(WdfGunwanLoger  record) {
		record.setId(UUIDUtils.getUUID());
		record.setCreatTime(new Date());
		return wdfGunwanLogerMapper.insert(record);
	}
	/**
	 * log列表
	 * @param pageNumber
	 * @param pageSize
	 * @param StartTime
	 * @param endTime
	 * @return
	 * @throws ParseException
	 */
	@Override
	public HashMap<String, Object> getGuanWanLogList(int pageNumber ,int pageSize ,String StartTime, String endTime ) throws ParseException {
	
		//总条数
		int total=0;
		//数据
		List<WdfGunwanLoger> list=null;
		try {
			WdfGunwanLogerExample example = new WdfGunwanLogerExample();
			Criteria criteria = example.createCriteria();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			if(StringUtils.isNotBlank(StartTime)&&StringUtils.isNotBlank(StartTime)) {
				Date date1 = formatter.parse(StartTime);
				Date date2 = formatter.parse(endTime);
				criteria.andCreatTimeBetween(date1, date2);
			}
			total = wdfGunwanLogerMapper.countByExample(example);
			//	List<WdfGunwanLoger> total = wdfGunwanLogerMapper.selectByExample(example);
			PageHelper.startPage(pageNumber, pageSize);
			example.setOrderByClause("creat_time");
			
			list = wdfGunwanLogerMapper.selectByExample(example);
		} catch (Exception e) {
			e.printStackTrace();
		}
		//封装数据
		HashMap<String, Object> result = new HashMap<String, Object>();
	    //请求次数
	    result.put("draw", 0);
	    //总共多少条数据 分页前
	    result.put("recordsTotal", total);
	    //过滤后的总数据 
	    result.put("recordsFiltered",total);
	    result.put("data", list);
	    return result;
	}
	/**
	 * 每天访问量
	 */
	@Override
	public Object selectEveryDayNum() {
		ArrayList<Object> datas=null;
		try {
			List<Map<String, Object>> map = wdfGunwanLogerMapper.selectEveryDayNum();
			ArrayList<Object> days = new ArrayList<Object>();
			ArrayList<Object> count = new ArrayList<Object>();
			if(map!=null) {
				for (Map<String, Object> map2 : map) {
					count.add(map2.get("number"));
					days.add(map2.get("days"));
				}
			}
			datas = new ArrayList<Object>();
			datas.add(days);
			datas.add(count);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return datas;
	}
	
}
