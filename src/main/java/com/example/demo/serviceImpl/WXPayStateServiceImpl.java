package com.example.demo.serviceImpl;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.springframework.stereotype.Service;

import com.example.demo.service.WXPayStateService;
import com.example.demo.wx.pay.HttpUtil;
import com.example.demo.wx.pay.PayConfigUtil;
import com.example.demo.wx.pay.PayToolUtil;
import com.github.wxpay.sdk.WXPayUtil;

@Service
public class WXPayStateServiceImpl implements WXPayStateService {

	@SuppressWarnings("unused")
	@Override
	public Map<String,String> getWxPayState(String out_trade_no) {
		SortedMap<Object, Object> packageParams = new TreeMap<Object, Object>();
		packageParams.put("appid", PayConfigUtil.APP_ID);
		packageParams.put("mch_id", PayConfigUtil.MCH_ID);
		packageParams.put("nonce_str", WXPayUtil.generateNonceStr());
		packageParams.put("out_trade_no", out_trade_no);
		String sign = PayToolUtil.createSign("UTF-8", packageParams, PayConfigUtil.API_KEY);
		packageParams.put("sign", sign);
		String requestXML = PayToolUtil.getRequestXml(packageParams);
		String resXml = HttpUtil.postData(PayConfigUtil.PAY_URL, requestXML);
		Map<String, String> map = null;
		try {
			map = WXPayUtil.xmlToMap(resXml);
			String tradeState = map.get("trade_state");
		} catch (Exception e) {
			e.printStackTrace();
		}

		return map;

	}

	public static void main(String[] args) {
		WXPayStateServiceImpl serviceImpl = new WXPayStateServiceImpl();
		Object object = serviceImpl.getWxPayState("1553330403552");
		System.out.println(object.toString());
	}
}
