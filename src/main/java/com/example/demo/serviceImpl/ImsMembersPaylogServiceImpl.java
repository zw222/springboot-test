package com.example.demo.serviceImpl;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.slaver.ImsMembersMapper;
import com.example.demo.dao.slaver.ImsMembersPaylogMapper;
import com.example.demo.dao.slaver.WdfPayLogerMapper;
import com.example.demo.pojo.ImsMembers;
import com.example.demo.pojo.ImsMembersPaylog;
import com.example.demo.pojo.WdfPayLoger;
/**
 * 支付日志
 */
import com.example.demo.service.ImsMembersPaylogService;

@Service
public class ImsMembersPaylogServiceImpl implements ImsMembersPaylogService {

	@Autowired
	private ImsMembersPaylogMapper imsMembersPaylogMapper;

	@Autowired
	private ImsMembersMapper imsMembersMapper;
	
	@Autowired
	private WdfPayLogerMapper wdfPayLogerMapper;

	/**
	 * 添加支付日志 //时间需要修改
	 * 
	 * @param record
	 */
	@Override
	public int insert(String userName , String outTradeNo) {
		int insert = 0;
			
		try {
			WdfPayLoger	log=wdfPayLogerMapper.selectByOutTradeNo(outTradeNo);
			ImsMembers members = imsMembersMapper.selectByUserName(userName);
			ImsMembersPaylog paylog = new ImsMembersPaylog();
			paylog.setMoney(new BigDecimal(""+log.getWxMoney()).divide(new BigDecimal("100")));
			paylog.setMsg("微信充值");
			paylog.setOrderId(0);
			// 时间需要修改
			paylog.setPaytime(getSecondTimestamp(new Date()));
			paylog.setStatus(1);
			paylog.setTopId(members.getTopId());
			paylog.setType(true);
			paylog.setUid(members.getUid());
			insert = imsMembersPaylogMapper.insert(paylog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return insert;
	}

	public static int getSecondTimestamp(Date date){
        if (null == date) {
            return 0;
        }
        String timestamp = String.valueOf(date.getTime());
        int length = timestamp.length();
        if (length > 3) {
            return Integer.valueOf(timestamp.substring(0,length-3));
        } else {
            return 0;
        }
    }

}
