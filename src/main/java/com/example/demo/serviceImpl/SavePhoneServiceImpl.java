package com.example.demo.serviceImpl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.master.PhoneMapper;
import com.example.demo.pojo.Phone;
import com.example.demo.service.SavePhoneService;

@Service
public class SavePhoneServiceImpl implements SavePhoneService {

	@Autowired
	private PhoneMapper phoneMapper;

	/**
	 * 保存手机号
	 */
	@Override
	public Integer Savephone(String phoneNumber) {
		Phone phone = new Phone();
		phone.setPhoneNumber(phoneNumber);
		phone.setCreateTime(new Date());
		int insert = 0;
		try {
			insert = phoneMapper.insert(phone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return insert;
	}

	/**
	 * 查寻手机号
	 */
	@Override
	public Map<String, Object> getPhoneList(int start, int length, Phone phone) {
		Map<String, Object> result = new HashMap<String, Object>();
		try {
			// 分页
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("phoneNumber", phone.getPhoneNumber());
			map.put("startTime", phone.getStartTime());
			map.put("endTime", phone.getEndTime());
			int total = phoneMapper.getAllTotalPhone(map);
			map.put("startRows", start);
			map.put("pageSize", length);
			List<Phone> list = phoneMapper.getPhoneList(map);
			// 请求次数
			result.put("draw", 0);
			// 总共多少条数据 分页前
			result.put("recordsTotal", total);
			// 过滤后的总数据
			result.put("recordsFiltered", total);
			result.put("data", list);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	
	
}
