package com.example.demo.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.master.WdfAdminUserMapper;
import com.example.demo.pojo.WdfAdminUser;
import com.example.demo.service.WdfAdminUserService;
@Service
public class WdfAdminUserServiceImpl implements WdfAdminUserService {

	@Autowired
	private WdfAdminUserMapper  wdfAdminUserMapper;
	/**
	 * 查寻用户
	 * @param name
	 */
	@Override
	public WdfAdminUser selectByName(String name) {
		WdfAdminUser user=null;
		try {
			user = wdfAdminUserMapper.selectByName(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}
}
