package com.example.demo.serviceImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.dao.master.WdfUserInfoMapper;
import com.example.demo.pojo.WdfGunwanLoger;
import com.example.demo.pojo.WdfGunwanLogerExample;
import com.example.demo.pojo.WdfUserInfo;
import com.example.demo.pojo.WdfUserInfoExample;
import com.example.demo.pojo.WdfUserInfoExample.Criteria;
import com.example.demo.service.WdfUserInfoService;
import com.example.demo.utils.UUIDUtils;
import com.github.pagehelper.PageHelper;

/**
 * 官网咨询记录
 * 
 * @author Hasee
 *
 */
@Service
public class WdfUserInfoServiceImpl implements WdfUserInfoService {

	@Autowired
	private WdfUserInfoMapper wdfUserInfoMapper;

	/**
	 * 添加咨询记录
	 */
	@Override
	public int insert(WdfUserInfo record) {
		record.setId(UUIDUtils.getUUID());
		record.setCreateTime(new Date());
		int insert = wdfUserInfoMapper.insert(record);
		return insert;
	}

	/**
	 * 查寻咨询列表
	 * 
	 * @throws ParseException
	 * 
	 */
	@Override
	public HashMap<String, Object> selectUserInfoList(int pageNumber, int pageSize, String StartTime, String endTime)
			throws ParseException {
		PageHelper.startPage(pageNumber, pageSize);
		WdfUserInfoExample example = new WdfUserInfoExample();
		Criteria criteria = example.createCriteria();
		if (StringUtils.isNotBlank(StartTime) && StringUtils.isNotBlank(StartTime)) {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date date1 = format.parse(StartTime);
			Date date2 = format.parse(endTime);
			criteria.andCreateTimeBetween(date1, date2);
		}
		// 总条数
		int total = wdfUserInfoMapper.countByExample(example);
		PageHelper.startPage(pageNumber, pageSize);
		example.setOrderByClause("create_time");
		// 数据
		List<WdfUserInfo> list = wdfUserInfoMapper.selectByExample(example);
		// 封装数据
		HashMap<String, Object> result = new HashMap<String, Object>();
		// 请求次数
		result.put("draw", 0);
		// 总共多少条数据 分页前
		result.put("recordsTotal", total);
		// 过滤后的总数据
		result.put("recordsFiltered", total);
		result.put("data", list);
		return result;
	}

}
