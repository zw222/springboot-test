package com.example.demo.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.dao.slaver.ImsMembersMapper;
import com.example.demo.pojo.ImsMembers;
import com.example.demo.service.ImsMembersService;

public class ImsMembersServiceImpl implements ImsMembersService {
	@Autowired
	private ImsMembersMapper imsMembersMapper;

	/**
	 * 更新用户money
	 * 
	 * @param record
	 */
	@Override
	public int upMemberMoney(ImsMembers record) {
		int i = 0;
		try {
			i = imsMembersMapper.updateByUserName(record);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;

	}

	/**
	 * 查寻用户
	 */
	@Override
	public ImsMembers selectByUserName(String userName) {
		ImsMembers member = imsMembersMapper.selectByUserName(userName);
		return member;
	}

}
