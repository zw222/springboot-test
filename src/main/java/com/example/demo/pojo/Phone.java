package com.example.demo.pojo;
import java.io.Serializable;import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * 
 * 
 * 
 **/
@SuppressWarnings("serial")
public class Phone implements Serializable {

	/**
	 *id
	 **/
	private long id;

	/**
	 *手机号码
	 **/
	private String phoneNumber;
	/**
	 *开始时间
	 **/
	@JsonIgnore
	private String startTime;
	/**
	 *结束时间
	 **/
	@JsonIgnore
	private String endTime;

	/**
	 *创建时间
	 **/
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private java.util.Date createTime;



	public void setId(long id){
		this.id = id;
	}

	public long getId(){
		return this.id;
	}

	public void setPhoneNumber(String phoneNumber){
		this.phoneNumber = phoneNumber;
	}

	public String getPhoneNumber(){
		return this.phoneNumber;
	}

	public void setCreateTime(java.util.Date createTime){
		this.createTime = createTime;
	}

	public java.util.Date getCreateTime(){
		return this.createTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	
}
