package com.example.demo.pojo;

public class ImsMembersWithBLOBs extends ImsMembers {
    private String configAll;

    private String actionList;

    public String getConfigAll() {
        return configAll;
    }

    public void setConfigAll(String configAll) {
        this.configAll = configAll == null ? null : configAll.trim();
    }

    public String getActionList() {
        return actionList;
    }

    public void setActionList(String actionList) {
        this.actionList = actionList == null ? null : actionList.trim();
    }
}