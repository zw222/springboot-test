package com.example.demo.pojo;

import java.math.BigDecimal;

public class ImsMembers {
    private Integer uid;

    private Integer groupid;

    private String username;

    private String realName;

    private String password;

    private String salt;

    private Byte status;

    private Integer joindate;

    private String joinip;

    private Integer lastvisit;

    private Integer did;

    private BigDecimal usemoney;

    private BigDecimal money;

    private String validtime;

    private String lastip;

    private String remark;

    private Integer parentId;

    private Integer msgNum;

    private String appKey;

    private Integer topId;

    private Integer pricegroupid;

    private Integer weid;

    private String token;

    private String roleId;

    private Integer feeType;

    private Integer overdraf;

    private BigDecimal price1;

    private BigDecimal price2;

    private BigDecimal phoneTime;

    private Integer phoneRoute;

    private String rewrite2;

    private String rewrite;

    private String areacode;

    private BigDecimal longPrice;

    private String returnUrl;

    private String recordRewrite;

    private Integer isRecord;

    private String audioNoticeUrl;

    private Integer msgAccount;

    private Integer smsId;

    private BigDecimal msgPrice;

    private Integer tjren;

    private Integer routeStatus;

    private Integer robotNumber;

    private Integer robotid;

    private String robotRoute;

    private String robotTaskid;

    private String robotGateway;

    private String robotDisplaynumber;

    private Integer callStatus;

    private BigDecimal operatorHuafeiBalance;

    private Integer decreaseType;

    private BigDecimal decreaseMiniteFee;

    private BigDecimal overspend;

    private Byte share;

    private Byte isshowphone;

    private String memberNo;

    private Integer isSystem;

    private BigDecimal bigdataCount;

    private Integer translateCount;

    private Integer isTranslate;

    private Integer robotTotal;

    private Integer isEditTemplate;

    private Integer isLimitCall;

    private Integer isCopyright;

    private String custom;

    private Integer useRobotNumber;

    private Integer todayTranslateCount;

    private Integer lineId;

    private Boolean ifOurs;

    private Integer limitCall;

    private Boolean isSection;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getGroupid() {
        return groupid;
    }

    public void setGroupid(Integer groupid) {
        this.groupid = groupid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName == null ? null : realName.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt == null ? null : salt.trim();
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Integer getJoindate() {
        return joindate;
    }

    public void setJoindate(Integer joindate) {
        this.joindate = joindate;
    }

    public String getJoinip() {
        return joinip;
    }

    public void setJoinip(String joinip) {
        this.joinip = joinip == null ? null : joinip.trim();
    }

    public Integer getLastvisit() {
        return lastvisit;
    }

    public void setLastvisit(Integer lastvisit) {
        this.lastvisit = lastvisit;
    }

    public Integer getDid() {
        return did;
    }

    public void setDid(Integer did) {
        this.did = did;
    }

    public BigDecimal getUsemoney() {
        return usemoney;
    }

    public void setUsemoney(BigDecimal usemoney) {
        this.usemoney = usemoney;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getValidtime() {
        return validtime;
    }

    public void setValidtime(String validtime) {
        this.validtime = validtime == null ? null : validtime.trim();
    }

    public String getLastip() {
        return lastip;
    }

    public void setLastip(String lastip) {
        this.lastip = lastip == null ? null : lastip.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getMsgNum() {
        return msgNum;
    }

    public void setMsgNum(Integer msgNum) {
        this.msgNum = msgNum;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey == null ? null : appKey.trim();
    }

    public Integer getTopId() {
        return topId;
    }

    public void setTopId(Integer topId) {
        this.topId = topId;
    }

    public Integer getPricegroupid() {
        return pricegroupid;
    }

    public void setPricegroupid(Integer pricegroupid) {
        this.pricegroupid = pricegroupid;
    }

    public Integer getWeid() {
        return weid;
    }

    public void setWeid(Integer weid) {
        this.weid = weid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId == null ? null : roleId.trim();
    }

    public Integer getFeeType() {
        return feeType;
    }

    public void setFeeType(Integer feeType) {
        this.feeType = feeType;
    }

    public Integer getOverdraf() {
        return overdraf;
    }

    public void setOverdraf(Integer overdraf) {
        this.overdraf = overdraf;
    }

    public BigDecimal getPrice1() {
        return price1;
    }

    public void setPrice1(BigDecimal price1) {
        this.price1 = price1;
    }

    public BigDecimal getPrice2() {
        return price2;
    }

    public void setPrice2(BigDecimal price2) {
        this.price2 = price2;
    }

    public BigDecimal getPhoneTime() {
        return phoneTime;
    }

    public void setPhoneTime(BigDecimal phoneTime) {
        this.phoneTime = phoneTime;
    }

    public Integer getPhoneRoute() {
        return phoneRoute;
    }

    public void setPhoneRoute(Integer phoneRoute) {
        this.phoneRoute = phoneRoute;
    }

    public String getRewrite2() {
        return rewrite2;
    }

    public void setRewrite2(String rewrite2) {
        this.rewrite2 = rewrite2 == null ? null : rewrite2.trim();
    }

    public String getRewrite() {
        return rewrite;
    }

    public void setRewrite(String rewrite) {
        this.rewrite = rewrite == null ? null : rewrite.trim();
    }

    public String getAreacode() {
        return areacode;
    }

    public void setAreacode(String areacode) {
        this.areacode = areacode == null ? null : areacode.trim();
    }

    public BigDecimal getLongPrice() {
        return longPrice;
    }

    public void setLongPrice(BigDecimal longPrice) {
        this.longPrice = longPrice;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl == null ? null : returnUrl.trim();
    }

    public String getRecordRewrite() {
        return recordRewrite;
    }

    public void setRecordRewrite(String recordRewrite) {
        this.recordRewrite = recordRewrite == null ? null : recordRewrite.trim();
    }

    public Integer getIsRecord() {
        return isRecord;
    }

    public void setIsRecord(Integer isRecord) {
        this.isRecord = isRecord;
    }

    public String getAudioNoticeUrl() {
        return audioNoticeUrl;
    }

    public void setAudioNoticeUrl(String audioNoticeUrl) {
        this.audioNoticeUrl = audioNoticeUrl == null ? null : audioNoticeUrl.trim();
    }

    public Integer getMsgAccount() {
        return msgAccount;
    }

    public void setMsgAccount(Integer msgAccount) {
        this.msgAccount = msgAccount;
    }

    public Integer getSmsId() {
        return smsId;
    }

    public void setSmsId(Integer smsId) {
        this.smsId = smsId;
    }

    public BigDecimal getMsgPrice() {
        return msgPrice;
    }

    public void setMsgPrice(BigDecimal msgPrice) {
        this.msgPrice = msgPrice;
    }

    public Integer getTjren() {
        return tjren;
    }

    public void setTjren(Integer tjren) {
        this.tjren = tjren;
    }

    public Integer getRouteStatus() {
        return routeStatus;
    }

    public void setRouteStatus(Integer routeStatus) {
        this.routeStatus = routeStatus;
    }

    public Integer getRobotNumber() {
        return robotNumber;
    }

    public void setRobotNumber(Integer robotNumber) {
        this.robotNumber = robotNumber;
    }

    public Integer getRobotid() {
        return robotid;
    }

    public void setRobotid(Integer robotid) {
        this.robotid = robotid;
    }

    public String getRobotRoute() {
        return robotRoute;
    }

    public void setRobotRoute(String robotRoute) {
        this.robotRoute = robotRoute == null ? null : robotRoute.trim();
    }

    public String getRobotTaskid() {
        return robotTaskid;
    }

    public void setRobotTaskid(String robotTaskid) {
        this.robotTaskid = robotTaskid == null ? null : robotTaskid.trim();
    }

    public String getRobotGateway() {
        return robotGateway;
    }

    public void setRobotGateway(String robotGateway) {
        this.robotGateway = robotGateway == null ? null : robotGateway.trim();
    }

    public String getRobotDisplaynumber() {
        return robotDisplaynumber;
    }

    public void setRobotDisplaynumber(String robotDisplaynumber) {
        this.robotDisplaynumber = robotDisplaynumber == null ? null : robotDisplaynumber.trim();
    }

    public Integer getCallStatus() {
        return callStatus;
    }

    public void setCallStatus(Integer callStatus) {
        this.callStatus = callStatus;
    }

    public BigDecimal getOperatorHuafeiBalance() {
        return operatorHuafeiBalance;
    }

    public void setOperatorHuafeiBalance(BigDecimal operatorHuafeiBalance) {
        this.operatorHuafeiBalance = operatorHuafeiBalance;
    }

    public Integer getDecreaseType() {
        return decreaseType;
    }

    public void setDecreaseType(Integer decreaseType) {
        this.decreaseType = decreaseType;
    }

    public BigDecimal getDecreaseMiniteFee() {
        return decreaseMiniteFee;
    }

    public void setDecreaseMiniteFee(BigDecimal decreaseMiniteFee) {
        this.decreaseMiniteFee = decreaseMiniteFee;
    }

    public BigDecimal getOverspend() {
        return overspend;
    }

    public void setOverspend(BigDecimal overspend) {
        this.overspend = overspend;
    }

    public Byte getShare() {
        return share;
    }

    public void setShare(Byte share) {
        this.share = share;
    }

    public Byte getIsshowphone() {
        return isshowphone;
    }

    public void setIsshowphone(Byte isshowphone) {
        this.isshowphone = isshowphone;
    }

    public String getMemberNo() {
        return memberNo;
    }

    public void setMemberNo(String memberNo) {
        this.memberNo = memberNo == null ? null : memberNo.trim();
    }

    public Integer getIsSystem() {
        return isSystem;
    }

    public void setIsSystem(Integer isSystem) {
        this.isSystem = isSystem;
    }

    public BigDecimal getBigdataCount() {
        return bigdataCount;
    }

    public void setBigdataCount(BigDecimal bigdataCount) {
        this.bigdataCount = bigdataCount;
    }

    public Integer getTranslateCount() {
        return translateCount;
    }

    public void setTranslateCount(Integer translateCount) {
        this.translateCount = translateCount;
    }

    public Integer getIsTranslate() {
        return isTranslate;
    }

    public void setIsTranslate(Integer isTranslate) {
        this.isTranslate = isTranslate;
    }

    public Integer getRobotTotal() {
        return robotTotal;
    }

    public void setRobotTotal(Integer robotTotal) {
        this.robotTotal = robotTotal;
    }

    public Integer getIsEditTemplate() {
        return isEditTemplate;
    }

    public void setIsEditTemplate(Integer isEditTemplate) {
        this.isEditTemplate = isEditTemplate;
    }

    public Integer getIsLimitCall() {
        return isLimitCall;
    }

    public void setIsLimitCall(Integer isLimitCall) {
        this.isLimitCall = isLimitCall;
    }

    public Integer getIsCopyright() {
        return isCopyright;
    }

    public void setIsCopyright(Integer isCopyright) {
        this.isCopyright = isCopyright;
    }

    public String getCustom() {
        return custom;
    }

    public void setCustom(String custom) {
        this.custom = custom == null ? null : custom.trim();
    }

    public Integer getUseRobotNumber() {
        return useRobotNumber;
    }

    public void setUseRobotNumber(Integer useRobotNumber) {
        this.useRobotNumber = useRobotNumber;
    }

    public Integer getTodayTranslateCount() {
        return todayTranslateCount;
    }

    public void setTodayTranslateCount(Integer todayTranslateCount) {
        this.todayTranslateCount = todayTranslateCount;
    }

    public Integer getLineId() {
        return lineId;
    }

    public void setLineId(Integer lineId) {
        this.lineId = lineId;
    }

    public Boolean getIfOurs() {
        return ifOurs;
    }

    public void setIfOurs(Boolean ifOurs) {
        this.ifOurs = ifOurs;
    }

    public Integer getLimitCall() {
        return limitCall;
    }

    public void setLimitCall(Integer limitCall) {
        this.limitCall = limitCall;
    }

    public Boolean getIsSection() {
        return isSection;
    }

    public void setIsSection(Boolean isSection) {
        this.isSection = isSection;
    }
}