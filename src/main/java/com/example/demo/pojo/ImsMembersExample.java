package com.example.demo.pojo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ImsMembersExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ImsMembersExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andUidIsNull() {
            addCriterion("uid is null");
            return (Criteria) this;
        }

        public Criteria andUidIsNotNull() {
            addCriterion("uid is not null");
            return (Criteria) this;
        }

        public Criteria andUidEqualTo(Integer value) {
            addCriterion("uid =", value, "uid");
            return (Criteria) this;
        }

        public Criteria andUidNotEqualTo(Integer value) {
            addCriterion("uid <>", value, "uid");
            return (Criteria) this;
        }

        public Criteria andUidGreaterThan(Integer value) {
            addCriterion("uid >", value, "uid");
            return (Criteria) this;
        }

        public Criteria andUidGreaterThanOrEqualTo(Integer value) {
            addCriterion("uid >=", value, "uid");
            return (Criteria) this;
        }

        public Criteria andUidLessThan(Integer value) {
            addCriterion("uid <", value, "uid");
            return (Criteria) this;
        }

        public Criteria andUidLessThanOrEqualTo(Integer value) {
            addCriterion("uid <=", value, "uid");
            return (Criteria) this;
        }

        public Criteria andUidIn(List<Integer> values) {
            addCriterion("uid in", values, "uid");
            return (Criteria) this;
        }

        public Criteria andUidNotIn(List<Integer> values) {
            addCriterion("uid not in", values, "uid");
            return (Criteria) this;
        }

        public Criteria andUidBetween(Integer value1, Integer value2) {
            addCriterion("uid between", value1, value2, "uid");
            return (Criteria) this;
        }

        public Criteria andUidNotBetween(Integer value1, Integer value2) {
            addCriterion("uid not between", value1, value2, "uid");
            return (Criteria) this;
        }

        public Criteria andGroupidIsNull() {
            addCriterion("groupid is null");
            return (Criteria) this;
        }

        public Criteria andGroupidIsNotNull() {
            addCriterion("groupid is not null");
            return (Criteria) this;
        }

        public Criteria andGroupidEqualTo(Integer value) {
            addCriterion("groupid =", value, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidNotEqualTo(Integer value) {
            addCriterion("groupid <>", value, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidGreaterThan(Integer value) {
            addCriterion("groupid >", value, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidGreaterThanOrEqualTo(Integer value) {
            addCriterion("groupid >=", value, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidLessThan(Integer value) {
            addCriterion("groupid <", value, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidLessThanOrEqualTo(Integer value) {
            addCriterion("groupid <=", value, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidIn(List<Integer> values) {
            addCriterion("groupid in", values, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidNotIn(List<Integer> values) {
            addCriterion("groupid not in", values, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidBetween(Integer value1, Integer value2) {
            addCriterion("groupid between", value1, value2, "groupid");
            return (Criteria) this;
        }

        public Criteria andGroupidNotBetween(Integer value1, Integer value2) {
            addCriterion("groupid not between", value1, value2, "groupid");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNull() {
            addCriterion("username is null");
            return (Criteria) this;
        }

        public Criteria andUsernameIsNotNull() {
            addCriterion("username is not null");
            return (Criteria) this;
        }

        public Criteria andUsernameEqualTo(String value) {
            addCriterion("username =", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotEqualTo(String value) {
            addCriterion("username <>", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThan(String value) {
            addCriterion("username >", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameGreaterThanOrEqualTo(String value) {
            addCriterion("username >=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThan(String value) {
            addCriterion("username <", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLessThanOrEqualTo(String value) {
            addCriterion("username <=", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameLike(String value) {
            addCriterion("username like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotLike(String value) {
            addCriterion("username not like", value, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameIn(List<String> values) {
            addCriterion("username in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotIn(List<String> values) {
            addCriterion("username not in", values, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameBetween(String value1, String value2) {
            addCriterion("username between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andUsernameNotBetween(String value1, String value2) {
            addCriterion("username not between", value1, value2, "username");
            return (Criteria) this;
        }

        public Criteria andRealNameIsNull() {
            addCriterion("real_name is null");
            return (Criteria) this;
        }

        public Criteria andRealNameIsNotNull() {
            addCriterion("real_name is not null");
            return (Criteria) this;
        }

        public Criteria andRealNameEqualTo(String value) {
            addCriterion("real_name =", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotEqualTo(String value) {
            addCriterion("real_name <>", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameGreaterThan(String value) {
            addCriterion("real_name >", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameGreaterThanOrEqualTo(String value) {
            addCriterion("real_name >=", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLessThan(String value) {
            addCriterion("real_name <", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLessThanOrEqualTo(String value) {
            addCriterion("real_name <=", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLike(String value) {
            addCriterion("real_name like", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotLike(String value) {
            addCriterion("real_name not like", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameIn(List<String> values) {
            addCriterion("real_name in", values, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotIn(List<String> values) {
            addCriterion("real_name not in", values, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameBetween(String value1, String value2) {
            addCriterion("real_name between", value1, value2, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotBetween(String value1, String value2) {
            addCriterion("real_name not between", value1, value2, "realName");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andSaltIsNull() {
            addCriterion("salt is null");
            return (Criteria) this;
        }

        public Criteria andSaltIsNotNull() {
            addCriterion("salt is not null");
            return (Criteria) this;
        }

        public Criteria andSaltEqualTo(String value) {
            addCriterion("salt =", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotEqualTo(String value) {
            addCriterion("salt <>", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltGreaterThan(String value) {
            addCriterion("salt >", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltGreaterThanOrEqualTo(String value) {
            addCriterion("salt >=", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLessThan(String value) {
            addCriterion("salt <", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLessThanOrEqualTo(String value) {
            addCriterion("salt <=", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLike(String value) {
            addCriterion("salt like", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotLike(String value) {
            addCriterion("salt not like", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltIn(List<String> values) {
            addCriterion("salt in", values, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotIn(List<String> values) {
            addCriterion("salt not in", values, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltBetween(String value1, String value2) {
            addCriterion("salt between", value1, value2, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotBetween(String value1, String value2) {
            addCriterion("salt not between", value1, value2, "salt");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andJoindateIsNull() {
            addCriterion("joindate is null");
            return (Criteria) this;
        }

        public Criteria andJoindateIsNotNull() {
            addCriterion("joindate is not null");
            return (Criteria) this;
        }

        public Criteria andJoindateEqualTo(Integer value) {
            addCriterion("joindate =", value, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateNotEqualTo(Integer value) {
            addCriterion("joindate <>", value, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateGreaterThan(Integer value) {
            addCriterion("joindate >", value, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateGreaterThanOrEqualTo(Integer value) {
            addCriterion("joindate >=", value, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateLessThan(Integer value) {
            addCriterion("joindate <", value, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateLessThanOrEqualTo(Integer value) {
            addCriterion("joindate <=", value, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateIn(List<Integer> values) {
            addCriterion("joindate in", values, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateNotIn(List<Integer> values) {
            addCriterion("joindate not in", values, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateBetween(Integer value1, Integer value2) {
            addCriterion("joindate between", value1, value2, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoindateNotBetween(Integer value1, Integer value2) {
            addCriterion("joindate not between", value1, value2, "joindate");
            return (Criteria) this;
        }

        public Criteria andJoinipIsNull() {
            addCriterion("joinip is null");
            return (Criteria) this;
        }

        public Criteria andJoinipIsNotNull() {
            addCriterion("joinip is not null");
            return (Criteria) this;
        }

        public Criteria andJoinipEqualTo(String value) {
            addCriterion("joinip =", value, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipNotEqualTo(String value) {
            addCriterion("joinip <>", value, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipGreaterThan(String value) {
            addCriterion("joinip >", value, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipGreaterThanOrEqualTo(String value) {
            addCriterion("joinip >=", value, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipLessThan(String value) {
            addCriterion("joinip <", value, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipLessThanOrEqualTo(String value) {
            addCriterion("joinip <=", value, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipLike(String value) {
            addCriterion("joinip like", value, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipNotLike(String value) {
            addCriterion("joinip not like", value, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipIn(List<String> values) {
            addCriterion("joinip in", values, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipNotIn(List<String> values) {
            addCriterion("joinip not in", values, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipBetween(String value1, String value2) {
            addCriterion("joinip between", value1, value2, "joinip");
            return (Criteria) this;
        }

        public Criteria andJoinipNotBetween(String value1, String value2) {
            addCriterion("joinip not between", value1, value2, "joinip");
            return (Criteria) this;
        }

        public Criteria andLastvisitIsNull() {
            addCriterion("lastvisit is null");
            return (Criteria) this;
        }

        public Criteria andLastvisitIsNotNull() {
            addCriterion("lastvisit is not null");
            return (Criteria) this;
        }

        public Criteria andLastvisitEqualTo(Integer value) {
            addCriterion("lastvisit =", value, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitNotEqualTo(Integer value) {
            addCriterion("lastvisit <>", value, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitGreaterThan(Integer value) {
            addCriterion("lastvisit >", value, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitGreaterThanOrEqualTo(Integer value) {
            addCriterion("lastvisit >=", value, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitLessThan(Integer value) {
            addCriterion("lastvisit <", value, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitLessThanOrEqualTo(Integer value) {
            addCriterion("lastvisit <=", value, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitIn(List<Integer> values) {
            addCriterion("lastvisit in", values, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitNotIn(List<Integer> values) {
            addCriterion("lastvisit not in", values, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitBetween(Integer value1, Integer value2) {
            addCriterion("lastvisit between", value1, value2, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andLastvisitNotBetween(Integer value1, Integer value2) {
            addCriterion("lastvisit not between", value1, value2, "lastvisit");
            return (Criteria) this;
        }

        public Criteria andDidIsNull() {
            addCriterion("did is null");
            return (Criteria) this;
        }

        public Criteria andDidIsNotNull() {
            addCriterion("did is not null");
            return (Criteria) this;
        }

        public Criteria andDidEqualTo(Integer value) {
            addCriterion("did =", value, "did");
            return (Criteria) this;
        }

        public Criteria andDidNotEqualTo(Integer value) {
            addCriterion("did <>", value, "did");
            return (Criteria) this;
        }

        public Criteria andDidGreaterThan(Integer value) {
            addCriterion("did >", value, "did");
            return (Criteria) this;
        }

        public Criteria andDidGreaterThanOrEqualTo(Integer value) {
            addCriterion("did >=", value, "did");
            return (Criteria) this;
        }

        public Criteria andDidLessThan(Integer value) {
            addCriterion("did <", value, "did");
            return (Criteria) this;
        }

        public Criteria andDidLessThanOrEqualTo(Integer value) {
            addCriterion("did <=", value, "did");
            return (Criteria) this;
        }

        public Criteria andDidIn(List<Integer> values) {
            addCriterion("did in", values, "did");
            return (Criteria) this;
        }

        public Criteria andDidNotIn(List<Integer> values) {
            addCriterion("did not in", values, "did");
            return (Criteria) this;
        }

        public Criteria andDidBetween(Integer value1, Integer value2) {
            addCriterion("did between", value1, value2, "did");
            return (Criteria) this;
        }

        public Criteria andDidNotBetween(Integer value1, Integer value2) {
            addCriterion("did not between", value1, value2, "did");
            return (Criteria) this;
        }

        public Criteria andUsemoneyIsNull() {
            addCriterion("usemoney is null");
            return (Criteria) this;
        }

        public Criteria andUsemoneyIsNotNull() {
            addCriterion("usemoney is not null");
            return (Criteria) this;
        }

        public Criteria andUsemoneyEqualTo(BigDecimal value) {
            addCriterion("usemoney =", value, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyNotEqualTo(BigDecimal value) {
            addCriterion("usemoney <>", value, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyGreaterThan(BigDecimal value) {
            addCriterion("usemoney >", value, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("usemoney >=", value, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyLessThan(BigDecimal value) {
            addCriterion("usemoney <", value, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("usemoney <=", value, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyIn(List<BigDecimal> values) {
            addCriterion("usemoney in", values, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyNotIn(List<BigDecimal> values) {
            addCriterion("usemoney not in", values, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("usemoney between", value1, value2, "usemoney");
            return (Criteria) this;
        }

        public Criteria andUsemoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("usemoney not between", value1, value2, "usemoney");
            return (Criteria) this;
        }

        public Criteria andMoneyIsNull() {
            addCriterion("money is null");
            return (Criteria) this;
        }

        public Criteria andMoneyIsNotNull() {
            addCriterion("money is not null");
            return (Criteria) this;
        }

        public Criteria andMoneyEqualTo(BigDecimal value) {
            addCriterion("money =", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotEqualTo(BigDecimal value) {
            addCriterion("money <>", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyGreaterThan(BigDecimal value) {
            addCriterion("money >", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("money >=", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyLessThan(BigDecimal value) {
            addCriterion("money <", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("money <=", value, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyIn(List<BigDecimal> values) {
            addCriterion("money in", values, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotIn(List<BigDecimal> values) {
            addCriterion("money not in", values, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("money between", value1, value2, "money");
            return (Criteria) this;
        }

        public Criteria andMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("money not between", value1, value2, "money");
            return (Criteria) this;
        }

        public Criteria andValidtimeIsNull() {
            addCriterion("validtime is null");
            return (Criteria) this;
        }

        public Criteria andValidtimeIsNotNull() {
            addCriterion("validtime is not null");
            return (Criteria) this;
        }

        public Criteria andValidtimeEqualTo(String value) {
            addCriterion("validtime =", value, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeNotEqualTo(String value) {
            addCriterion("validtime <>", value, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeGreaterThan(String value) {
            addCriterion("validtime >", value, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeGreaterThanOrEqualTo(String value) {
            addCriterion("validtime >=", value, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeLessThan(String value) {
            addCriterion("validtime <", value, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeLessThanOrEqualTo(String value) {
            addCriterion("validtime <=", value, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeLike(String value) {
            addCriterion("validtime like", value, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeNotLike(String value) {
            addCriterion("validtime not like", value, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeIn(List<String> values) {
            addCriterion("validtime in", values, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeNotIn(List<String> values) {
            addCriterion("validtime not in", values, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeBetween(String value1, String value2) {
            addCriterion("validtime between", value1, value2, "validtime");
            return (Criteria) this;
        }

        public Criteria andValidtimeNotBetween(String value1, String value2) {
            addCriterion("validtime not between", value1, value2, "validtime");
            return (Criteria) this;
        }

        public Criteria andLastipIsNull() {
            addCriterion("lastip is null");
            return (Criteria) this;
        }

        public Criteria andLastipIsNotNull() {
            addCriterion("lastip is not null");
            return (Criteria) this;
        }

        public Criteria andLastipEqualTo(String value) {
            addCriterion("lastip =", value, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipNotEqualTo(String value) {
            addCriterion("lastip <>", value, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipGreaterThan(String value) {
            addCriterion("lastip >", value, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipGreaterThanOrEqualTo(String value) {
            addCriterion("lastip >=", value, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipLessThan(String value) {
            addCriterion("lastip <", value, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipLessThanOrEqualTo(String value) {
            addCriterion("lastip <=", value, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipLike(String value) {
            addCriterion("lastip like", value, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipNotLike(String value) {
            addCriterion("lastip not like", value, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipIn(List<String> values) {
            addCriterion("lastip in", values, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipNotIn(List<String> values) {
            addCriterion("lastip not in", values, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipBetween(String value1, String value2) {
            addCriterion("lastip between", value1, value2, "lastip");
            return (Criteria) this;
        }

        public Criteria andLastipNotBetween(String value1, String value2) {
            addCriterion("lastip not between", value1, value2, "lastip");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("parent_id is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(Integer value) {
            addCriterion("parent_id =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(Integer value) {
            addCriterion("parent_id <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(Integer value) {
            addCriterion("parent_id >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("parent_id >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(Integer value) {
            addCriterion("parent_id <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("parent_id <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<Integer> values) {
            addCriterion("parent_id in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<Integer> values) {
            addCriterion("parent_id not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(Integer value1, Integer value2) {
            addCriterion("parent_id between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("parent_id not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andMsgNumIsNull() {
            addCriterion("msg_num is null");
            return (Criteria) this;
        }

        public Criteria andMsgNumIsNotNull() {
            addCriterion("msg_num is not null");
            return (Criteria) this;
        }

        public Criteria andMsgNumEqualTo(Integer value) {
            addCriterion("msg_num =", value, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumNotEqualTo(Integer value) {
            addCriterion("msg_num <>", value, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumGreaterThan(Integer value) {
            addCriterion("msg_num >", value, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("msg_num >=", value, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumLessThan(Integer value) {
            addCriterion("msg_num <", value, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumLessThanOrEqualTo(Integer value) {
            addCriterion("msg_num <=", value, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumIn(List<Integer> values) {
            addCriterion("msg_num in", values, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumNotIn(List<Integer> values) {
            addCriterion("msg_num not in", values, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumBetween(Integer value1, Integer value2) {
            addCriterion("msg_num between", value1, value2, "msgNum");
            return (Criteria) this;
        }

        public Criteria andMsgNumNotBetween(Integer value1, Integer value2) {
            addCriterion("msg_num not between", value1, value2, "msgNum");
            return (Criteria) this;
        }

        public Criteria andAppKeyIsNull() {
            addCriterion("app_key is null");
            return (Criteria) this;
        }

        public Criteria andAppKeyIsNotNull() {
            addCriterion("app_key is not null");
            return (Criteria) this;
        }

        public Criteria andAppKeyEqualTo(String value) {
            addCriterion("app_key =", value, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyNotEqualTo(String value) {
            addCriterion("app_key <>", value, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyGreaterThan(String value) {
            addCriterion("app_key >", value, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyGreaterThanOrEqualTo(String value) {
            addCriterion("app_key >=", value, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyLessThan(String value) {
            addCriterion("app_key <", value, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyLessThanOrEqualTo(String value) {
            addCriterion("app_key <=", value, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyLike(String value) {
            addCriterion("app_key like", value, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyNotLike(String value) {
            addCriterion("app_key not like", value, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyIn(List<String> values) {
            addCriterion("app_key in", values, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyNotIn(List<String> values) {
            addCriterion("app_key not in", values, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyBetween(String value1, String value2) {
            addCriterion("app_key between", value1, value2, "appKey");
            return (Criteria) this;
        }

        public Criteria andAppKeyNotBetween(String value1, String value2) {
            addCriterion("app_key not between", value1, value2, "appKey");
            return (Criteria) this;
        }

        public Criteria andTopIdIsNull() {
            addCriterion("top_id is null");
            return (Criteria) this;
        }

        public Criteria andTopIdIsNotNull() {
            addCriterion("top_id is not null");
            return (Criteria) this;
        }

        public Criteria andTopIdEqualTo(Integer value) {
            addCriterion("top_id =", value, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdNotEqualTo(Integer value) {
            addCriterion("top_id <>", value, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdGreaterThan(Integer value) {
            addCriterion("top_id >", value, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("top_id >=", value, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdLessThan(Integer value) {
            addCriterion("top_id <", value, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdLessThanOrEqualTo(Integer value) {
            addCriterion("top_id <=", value, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdIn(List<Integer> values) {
            addCriterion("top_id in", values, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdNotIn(List<Integer> values) {
            addCriterion("top_id not in", values, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdBetween(Integer value1, Integer value2) {
            addCriterion("top_id between", value1, value2, "topId");
            return (Criteria) this;
        }

        public Criteria andTopIdNotBetween(Integer value1, Integer value2) {
            addCriterion("top_id not between", value1, value2, "topId");
            return (Criteria) this;
        }

        public Criteria andPricegroupidIsNull() {
            addCriterion("pricegroupid is null");
            return (Criteria) this;
        }

        public Criteria andPricegroupidIsNotNull() {
            addCriterion("pricegroupid is not null");
            return (Criteria) this;
        }

        public Criteria andPricegroupidEqualTo(Integer value) {
            addCriterion("pricegroupid =", value, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidNotEqualTo(Integer value) {
            addCriterion("pricegroupid <>", value, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidGreaterThan(Integer value) {
            addCriterion("pricegroupid >", value, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidGreaterThanOrEqualTo(Integer value) {
            addCriterion("pricegroupid >=", value, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidLessThan(Integer value) {
            addCriterion("pricegroupid <", value, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidLessThanOrEqualTo(Integer value) {
            addCriterion("pricegroupid <=", value, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidIn(List<Integer> values) {
            addCriterion("pricegroupid in", values, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidNotIn(List<Integer> values) {
            addCriterion("pricegroupid not in", values, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidBetween(Integer value1, Integer value2) {
            addCriterion("pricegroupid between", value1, value2, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andPricegroupidNotBetween(Integer value1, Integer value2) {
            addCriterion("pricegroupid not between", value1, value2, "pricegroupid");
            return (Criteria) this;
        }

        public Criteria andWeidIsNull() {
            addCriterion("weid is null");
            return (Criteria) this;
        }

        public Criteria andWeidIsNotNull() {
            addCriterion("weid is not null");
            return (Criteria) this;
        }

        public Criteria andWeidEqualTo(Integer value) {
            addCriterion("weid =", value, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidNotEqualTo(Integer value) {
            addCriterion("weid <>", value, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidGreaterThan(Integer value) {
            addCriterion("weid >", value, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidGreaterThanOrEqualTo(Integer value) {
            addCriterion("weid >=", value, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidLessThan(Integer value) {
            addCriterion("weid <", value, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidLessThanOrEqualTo(Integer value) {
            addCriterion("weid <=", value, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidIn(List<Integer> values) {
            addCriterion("weid in", values, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidNotIn(List<Integer> values) {
            addCriterion("weid not in", values, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidBetween(Integer value1, Integer value2) {
            addCriterion("weid between", value1, value2, "weid");
            return (Criteria) this;
        }

        public Criteria andWeidNotBetween(Integer value1, Integer value2) {
            addCriterion("weid not between", value1, value2, "weid");
            return (Criteria) this;
        }

        public Criteria andTokenIsNull() {
            addCriterion("token is null");
            return (Criteria) this;
        }

        public Criteria andTokenIsNotNull() {
            addCriterion("token is not null");
            return (Criteria) this;
        }

        public Criteria andTokenEqualTo(String value) {
            addCriterion("token =", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenNotEqualTo(String value) {
            addCriterion("token <>", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenGreaterThan(String value) {
            addCriterion("token >", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenGreaterThanOrEqualTo(String value) {
            addCriterion("token >=", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenLessThan(String value) {
            addCriterion("token <", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenLessThanOrEqualTo(String value) {
            addCriterion("token <=", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenLike(String value) {
            addCriterion("token like", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenNotLike(String value) {
            addCriterion("token not like", value, "token");
            return (Criteria) this;
        }

        public Criteria andTokenIn(List<String> values) {
            addCriterion("token in", values, "token");
            return (Criteria) this;
        }

        public Criteria andTokenNotIn(List<String> values) {
            addCriterion("token not in", values, "token");
            return (Criteria) this;
        }

        public Criteria andTokenBetween(String value1, String value2) {
            addCriterion("token between", value1, value2, "token");
            return (Criteria) this;
        }

        public Criteria andTokenNotBetween(String value1, String value2) {
            addCriterion("token not between", value1, value2, "token");
            return (Criteria) this;
        }

        public Criteria andRoleIdIsNull() {
            addCriterion("role_id is null");
            return (Criteria) this;
        }

        public Criteria andRoleIdIsNotNull() {
            addCriterion("role_id is not null");
            return (Criteria) this;
        }

        public Criteria andRoleIdEqualTo(String value) {
            addCriterion("role_id =", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdNotEqualTo(String value) {
            addCriterion("role_id <>", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdGreaterThan(String value) {
            addCriterion("role_id >", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdGreaterThanOrEqualTo(String value) {
            addCriterion("role_id >=", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdLessThan(String value) {
            addCriterion("role_id <", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdLessThanOrEqualTo(String value) {
            addCriterion("role_id <=", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdLike(String value) {
            addCriterion("role_id like", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdNotLike(String value) {
            addCriterion("role_id not like", value, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdIn(List<String> values) {
            addCriterion("role_id in", values, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdNotIn(List<String> values) {
            addCriterion("role_id not in", values, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdBetween(String value1, String value2) {
            addCriterion("role_id between", value1, value2, "roleId");
            return (Criteria) this;
        }

        public Criteria andRoleIdNotBetween(String value1, String value2) {
            addCriterion("role_id not between", value1, value2, "roleId");
            return (Criteria) this;
        }

        public Criteria andFeeTypeIsNull() {
            addCriterion("fee_type is null");
            return (Criteria) this;
        }

        public Criteria andFeeTypeIsNotNull() {
            addCriterion("fee_type is not null");
            return (Criteria) this;
        }

        public Criteria andFeeTypeEqualTo(Integer value) {
            addCriterion("fee_type =", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeNotEqualTo(Integer value) {
            addCriterion("fee_type <>", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeGreaterThan(Integer value) {
            addCriterion("fee_type >", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("fee_type >=", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeLessThan(Integer value) {
            addCriterion("fee_type <", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeLessThanOrEqualTo(Integer value) {
            addCriterion("fee_type <=", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeIn(List<Integer> values) {
            addCriterion("fee_type in", values, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeNotIn(List<Integer> values) {
            addCriterion("fee_type not in", values, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeBetween(Integer value1, Integer value2) {
            addCriterion("fee_type between", value1, value2, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("fee_type not between", value1, value2, "feeType");
            return (Criteria) this;
        }

        public Criteria andOverdrafIsNull() {
            addCriterion("overdraf is null");
            return (Criteria) this;
        }

        public Criteria andOverdrafIsNotNull() {
            addCriterion("overdraf is not null");
            return (Criteria) this;
        }

        public Criteria andOverdrafEqualTo(Integer value) {
            addCriterion("overdraf =", value, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafNotEqualTo(Integer value) {
            addCriterion("overdraf <>", value, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafGreaterThan(Integer value) {
            addCriterion("overdraf >", value, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafGreaterThanOrEqualTo(Integer value) {
            addCriterion("overdraf >=", value, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafLessThan(Integer value) {
            addCriterion("overdraf <", value, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafLessThanOrEqualTo(Integer value) {
            addCriterion("overdraf <=", value, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafIn(List<Integer> values) {
            addCriterion("overdraf in", values, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafNotIn(List<Integer> values) {
            addCriterion("overdraf not in", values, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafBetween(Integer value1, Integer value2) {
            addCriterion("overdraf between", value1, value2, "overdraf");
            return (Criteria) this;
        }

        public Criteria andOverdrafNotBetween(Integer value1, Integer value2) {
            addCriterion("overdraf not between", value1, value2, "overdraf");
            return (Criteria) this;
        }

        public Criteria andPrice1IsNull() {
            addCriterion("price1 is null");
            return (Criteria) this;
        }

        public Criteria andPrice1IsNotNull() {
            addCriterion("price1 is not null");
            return (Criteria) this;
        }

        public Criteria andPrice1EqualTo(BigDecimal value) {
            addCriterion("price1 =", value, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1NotEqualTo(BigDecimal value) {
            addCriterion("price1 <>", value, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1GreaterThan(BigDecimal value) {
            addCriterion("price1 >", value, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price1 >=", value, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1LessThan(BigDecimal value) {
            addCriterion("price1 <", value, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1LessThanOrEqualTo(BigDecimal value) {
            addCriterion("price1 <=", value, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1In(List<BigDecimal> values) {
            addCriterion("price1 in", values, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1NotIn(List<BigDecimal> values) {
            addCriterion("price1 not in", values, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("price1 between", value1, value2, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice1NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price1 not between", value1, value2, "price1");
            return (Criteria) this;
        }

        public Criteria andPrice2IsNull() {
            addCriterion("price2 is null");
            return (Criteria) this;
        }

        public Criteria andPrice2IsNotNull() {
            addCriterion("price2 is not null");
            return (Criteria) this;
        }

        public Criteria andPrice2EqualTo(BigDecimal value) {
            addCriterion("price2 =", value, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2NotEqualTo(BigDecimal value) {
            addCriterion("price2 <>", value, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2GreaterThan(BigDecimal value) {
            addCriterion("price2 >", value, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("price2 >=", value, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2LessThan(BigDecimal value) {
            addCriterion("price2 <", value, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2LessThanOrEqualTo(BigDecimal value) {
            addCriterion("price2 <=", value, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2In(List<BigDecimal> values) {
            addCriterion("price2 in", values, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2NotIn(List<BigDecimal> values) {
            addCriterion("price2 not in", values, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("price2 between", value1, value2, "price2");
            return (Criteria) this;
        }

        public Criteria andPrice2NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("price2 not between", value1, value2, "price2");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeIsNull() {
            addCriterion("phone_time is null");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeIsNotNull() {
            addCriterion("phone_time is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeEqualTo(BigDecimal value) {
            addCriterion("phone_time =", value, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeNotEqualTo(BigDecimal value) {
            addCriterion("phone_time <>", value, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeGreaterThan(BigDecimal value) {
            addCriterion("phone_time >", value, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("phone_time >=", value, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeLessThan(BigDecimal value) {
            addCriterion("phone_time <", value, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("phone_time <=", value, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeIn(List<BigDecimal> values) {
            addCriterion("phone_time in", values, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeNotIn(List<BigDecimal> values) {
            addCriterion("phone_time not in", values, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("phone_time between", value1, value2, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneTimeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("phone_time not between", value1, value2, "phoneTime");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteIsNull() {
            addCriterion("phone_route is null");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteIsNotNull() {
            addCriterion("phone_route is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteEqualTo(Integer value) {
            addCriterion("phone_route =", value, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteNotEqualTo(Integer value) {
            addCriterion("phone_route <>", value, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteGreaterThan(Integer value) {
            addCriterion("phone_route >", value, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteGreaterThanOrEqualTo(Integer value) {
            addCriterion("phone_route >=", value, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteLessThan(Integer value) {
            addCriterion("phone_route <", value, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteLessThanOrEqualTo(Integer value) {
            addCriterion("phone_route <=", value, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteIn(List<Integer> values) {
            addCriterion("phone_route in", values, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteNotIn(List<Integer> values) {
            addCriterion("phone_route not in", values, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteBetween(Integer value1, Integer value2) {
            addCriterion("phone_route between", value1, value2, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andPhoneRouteNotBetween(Integer value1, Integer value2) {
            addCriterion("phone_route not between", value1, value2, "phoneRoute");
            return (Criteria) this;
        }

        public Criteria andRewrite2IsNull() {
            addCriterion("rewrite2 is null");
            return (Criteria) this;
        }

        public Criteria andRewrite2IsNotNull() {
            addCriterion("rewrite2 is not null");
            return (Criteria) this;
        }

        public Criteria andRewrite2EqualTo(String value) {
            addCriterion("rewrite2 =", value, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2NotEqualTo(String value) {
            addCriterion("rewrite2 <>", value, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2GreaterThan(String value) {
            addCriterion("rewrite2 >", value, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2GreaterThanOrEqualTo(String value) {
            addCriterion("rewrite2 >=", value, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2LessThan(String value) {
            addCriterion("rewrite2 <", value, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2LessThanOrEqualTo(String value) {
            addCriterion("rewrite2 <=", value, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2Like(String value) {
            addCriterion("rewrite2 like", value, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2NotLike(String value) {
            addCriterion("rewrite2 not like", value, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2In(List<String> values) {
            addCriterion("rewrite2 in", values, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2NotIn(List<String> values) {
            addCriterion("rewrite2 not in", values, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2Between(String value1, String value2) {
            addCriterion("rewrite2 between", value1, value2, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewrite2NotBetween(String value1, String value2) {
            addCriterion("rewrite2 not between", value1, value2, "rewrite2");
            return (Criteria) this;
        }

        public Criteria andRewriteIsNull() {
            addCriterion("rewrite is null");
            return (Criteria) this;
        }

        public Criteria andRewriteIsNotNull() {
            addCriterion("rewrite is not null");
            return (Criteria) this;
        }

        public Criteria andRewriteEqualTo(String value) {
            addCriterion("rewrite =", value, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteNotEqualTo(String value) {
            addCriterion("rewrite <>", value, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteGreaterThan(String value) {
            addCriterion("rewrite >", value, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteGreaterThanOrEqualTo(String value) {
            addCriterion("rewrite >=", value, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteLessThan(String value) {
            addCriterion("rewrite <", value, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteLessThanOrEqualTo(String value) {
            addCriterion("rewrite <=", value, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteLike(String value) {
            addCriterion("rewrite like", value, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteNotLike(String value) {
            addCriterion("rewrite not like", value, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteIn(List<String> values) {
            addCriterion("rewrite in", values, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteNotIn(List<String> values) {
            addCriterion("rewrite not in", values, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteBetween(String value1, String value2) {
            addCriterion("rewrite between", value1, value2, "rewrite");
            return (Criteria) this;
        }

        public Criteria andRewriteNotBetween(String value1, String value2) {
            addCriterion("rewrite not between", value1, value2, "rewrite");
            return (Criteria) this;
        }

        public Criteria andAreacodeIsNull() {
            addCriterion("areaCode is null");
            return (Criteria) this;
        }

        public Criteria andAreacodeIsNotNull() {
            addCriterion("areaCode is not null");
            return (Criteria) this;
        }

        public Criteria andAreacodeEqualTo(String value) {
            addCriterion("areaCode =", value, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeNotEqualTo(String value) {
            addCriterion("areaCode <>", value, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeGreaterThan(String value) {
            addCriterion("areaCode >", value, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeGreaterThanOrEqualTo(String value) {
            addCriterion("areaCode >=", value, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeLessThan(String value) {
            addCriterion("areaCode <", value, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeLessThanOrEqualTo(String value) {
            addCriterion("areaCode <=", value, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeLike(String value) {
            addCriterion("areaCode like", value, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeNotLike(String value) {
            addCriterion("areaCode not like", value, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeIn(List<String> values) {
            addCriterion("areaCode in", values, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeNotIn(List<String> values) {
            addCriterion("areaCode not in", values, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeBetween(String value1, String value2) {
            addCriterion("areaCode between", value1, value2, "areacode");
            return (Criteria) this;
        }

        public Criteria andAreacodeNotBetween(String value1, String value2) {
            addCriterion("areaCode not between", value1, value2, "areacode");
            return (Criteria) this;
        }

        public Criteria andLongPriceIsNull() {
            addCriterion("long_price is null");
            return (Criteria) this;
        }

        public Criteria andLongPriceIsNotNull() {
            addCriterion("long_price is not null");
            return (Criteria) this;
        }

        public Criteria andLongPriceEqualTo(BigDecimal value) {
            addCriterion("long_price =", value, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceNotEqualTo(BigDecimal value) {
            addCriterion("long_price <>", value, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceGreaterThan(BigDecimal value) {
            addCriterion("long_price >", value, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("long_price >=", value, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceLessThan(BigDecimal value) {
            addCriterion("long_price <", value, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("long_price <=", value, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceIn(List<BigDecimal> values) {
            addCriterion("long_price in", values, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceNotIn(List<BigDecimal> values) {
            addCriterion("long_price not in", values, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("long_price between", value1, value2, "longPrice");
            return (Criteria) this;
        }

        public Criteria andLongPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("long_price not between", value1, value2, "longPrice");
            return (Criteria) this;
        }

        public Criteria andReturnUrlIsNull() {
            addCriterion("return_url is null");
            return (Criteria) this;
        }

        public Criteria andReturnUrlIsNotNull() {
            addCriterion("return_url is not null");
            return (Criteria) this;
        }

        public Criteria andReturnUrlEqualTo(String value) {
            addCriterion("return_url =", value, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlNotEqualTo(String value) {
            addCriterion("return_url <>", value, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlGreaterThan(String value) {
            addCriterion("return_url >", value, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlGreaterThanOrEqualTo(String value) {
            addCriterion("return_url >=", value, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlLessThan(String value) {
            addCriterion("return_url <", value, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlLessThanOrEqualTo(String value) {
            addCriterion("return_url <=", value, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlLike(String value) {
            addCriterion("return_url like", value, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlNotLike(String value) {
            addCriterion("return_url not like", value, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlIn(List<String> values) {
            addCriterion("return_url in", values, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlNotIn(List<String> values) {
            addCriterion("return_url not in", values, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlBetween(String value1, String value2) {
            addCriterion("return_url between", value1, value2, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andReturnUrlNotBetween(String value1, String value2) {
            addCriterion("return_url not between", value1, value2, "returnUrl");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteIsNull() {
            addCriterion("record_rewrite is null");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteIsNotNull() {
            addCriterion("record_rewrite is not null");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteEqualTo(String value) {
            addCriterion("record_rewrite =", value, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteNotEqualTo(String value) {
            addCriterion("record_rewrite <>", value, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteGreaterThan(String value) {
            addCriterion("record_rewrite >", value, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteGreaterThanOrEqualTo(String value) {
            addCriterion("record_rewrite >=", value, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteLessThan(String value) {
            addCriterion("record_rewrite <", value, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteLessThanOrEqualTo(String value) {
            addCriterion("record_rewrite <=", value, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteLike(String value) {
            addCriterion("record_rewrite like", value, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteNotLike(String value) {
            addCriterion("record_rewrite not like", value, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteIn(List<String> values) {
            addCriterion("record_rewrite in", values, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteNotIn(List<String> values) {
            addCriterion("record_rewrite not in", values, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteBetween(String value1, String value2) {
            addCriterion("record_rewrite between", value1, value2, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andRecordRewriteNotBetween(String value1, String value2) {
            addCriterion("record_rewrite not between", value1, value2, "recordRewrite");
            return (Criteria) this;
        }

        public Criteria andIsRecordIsNull() {
            addCriterion("is_record is null");
            return (Criteria) this;
        }

        public Criteria andIsRecordIsNotNull() {
            addCriterion("is_record is not null");
            return (Criteria) this;
        }

        public Criteria andIsRecordEqualTo(Integer value) {
            addCriterion("is_record =", value, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordNotEqualTo(Integer value) {
            addCriterion("is_record <>", value, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordGreaterThan(Integer value) {
            addCriterion("is_record >", value, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_record >=", value, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordLessThan(Integer value) {
            addCriterion("is_record <", value, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordLessThanOrEqualTo(Integer value) {
            addCriterion("is_record <=", value, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordIn(List<Integer> values) {
            addCriterion("is_record in", values, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordNotIn(List<Integer> values) {
            addCriterion("is_record not in", values, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordBetween(Integer value1, Integer value2) {
            addCriterion("is_record between", value1, value2, "isRecord");
            return (Criteria) this;
        }

        public Criteria andIsRecordNotBetween(Integer value1, Integer value2) {
            addCriterion("is_record not between", value1, value2, "isRecord");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlIsNull() {
            addCriterion("audio_notice_url is null");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlIsNotNull() {
            addCriterion("audio_notice_url is not null");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlEqualTo(String value) {
            addCriterion("audio_notice_url =", value, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlNotEqualTo(String value) {
            addCriterion("audio_notice_url <>", value, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlGreaterThan(String value) {
            addCriterion("audio_notice_url >", value, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlGreaterThanOrEqualTo(String value) {
            addCriterion("audio_notice_url >=", value, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlLessThan(String value) {
            addCriterion("audio_notice_url <", value, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlLessThanOrEqualTo(String value) {
            addCriterion("audio_notice_url <=", value, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlLike(String value) {
            addCriterion("audio_notice_url like", value, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlNotLike(String value) {
            addCriterion("audio_notice_url not like", value, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlIn(List<String> values) {
            addCriterion("audio_notice_url in", values, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlNotIn(List<String> values) {
            addCriterion("audio_notice_url not in", values, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlBetween(String value1, String value2) {
            addCriterion("audio_notice_url between", value1, value2, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andAudioNoticeUrlNotBetween(String value1, String value2) {
            addCriterion("audio_notice_url not between", value1, value2, "audioNoticeUrl");
            return (Criteria) this;
        }

        public Criteria andMsgAccountIsNull() {
            addCriterion("msg_account is null");
            return (Criteria) this;
        }

        public Criteria andMsgAccountIsNotNull() {
            addCriterion("msg_account is not null");
            return (Criteria) this;
        }

        public Criteria andMsgAccountEqualTo(Integer value) {
            addCriterion("msg_account =", value, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountNotEqualTo(Integer value) {
            addCriterion("msg_account <>", value, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountGreaterThan(Integer value) {
            addCriterion("msg_account >", value, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountGreaterThanOrEqualTo(Integer value) {
            addCriterion("msg_account >=", value, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountLessThan(Integer value) {
            addCriterion("msg_account <", value, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountLessThanOrEqualTo(Integer value) {
            addCriterion("msg_account <=", value, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountIn(List<Integer> values) {
            addCriterion("msg_account in", values, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountNotIn(List<Integer> values) {
            addCriterion("msg_account not in", values, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountBetween(Integer value1, Integer value2) {
            addCriterion("msg_account between", value1, value2, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andMsgAccountNotBetween(Integer value1, Integer value2) {
            addCriterion("msg_account not between", value1, value2, "msgAccount");
            return (Criteria) this;
        }

        public Criteria andSmsIdIsNull() {
            addCriterion("sms_id is null");
            return (Criteria) this;
        }

        public Criteria andSmsIdIsNotNull() {
            addCriterion("sms_id is not null");
            return (Criteria) this;
        }

        public Criteria andSmsIdEqualTo(Integer value) {
            addCriterion("sms_id =", value, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdNotEqualTo(Integer value) {
            addCriterion("sms_id <>", value, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdGreaterThan(Integer value) {
            addCriterion("sms_id >", value, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("sms_id >=", value, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdLessThan(Integer value) {
            addCriterion("sms_id <", value, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdLessThanOrEqualTo(Integer value) {
            addCriterion("sms_id <=", value, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdIn(List<Integer> values) {
            addCriterion("sms_id in", values, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdNotIn(List<Integer> values) {
            addCriterion("sms_id not in", values, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdBetween(Integer value1, Integer value2) {
            addCriterion("sms_id between", value1, value2, "smsId");
            return (Criteria) this;
        }

        public Criteria andSmsIdNotBetween(Integer value1, Integer value2) {
            addCriterion("sms_id not between", value1, value2, "smsId");
            return (Criteria) this;
        }

        public Criteria andMsgPriceIsNull() {
            addCriterion("msg_price is null");
            return (Criteria) this;
        }

        public Criteria andMsgPriceIsNotNull() {
            addCriterion("msg_price is not null");
            return (Criteria) this;
        }

        public Criteria andMsgPriceEqualTo(BigDecimal value) {
            addCriterion("msg_price =", value, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceNotEqualTo(BigDecimal value) {
            addCriterion("msg_price <>", value, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceGreaterThan(BigDecimal value) {
            addCriterion("msg_price >", value, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("msg_price >=", value, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceLessThan(BigDecimal value) {
            addCriterion("msg_price <", value, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("msg_price <=", value, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceIn(List<BigDecimal> values) {
            addCriterion("msg_price in", values, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceNotIn(List<BigDecimal> values) {
            addCriterion("msg_price not in", values, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("msg_price between", value1, value2, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andMsgPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("msg_price not between", value1, value2, "msgPrice");
            return (Criteria) this;
        }

        public Criteria andTjrenIsNull() {
            addCriterion("tjren is null");
            return (Criteria) this;
        }

        public Criteria andTjrenIsNotNull() {
            addCriterion("tjren is not null");
            return (Criteria) this;
        }

        public Criteria andTjrenEqualTo(Integer value) {
            addCriterion("tjren =", value, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenNotEqualTo(Integer value) {
            addCriterion("tjren <>", value, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenGreaterThan(Integer value) {
            addCriterion("tjren >", value, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenGreaterThanOrEqualTo(Integer value) {
            addCriterion("tjren >=", value, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenLessThan(Integer value) {
            addCriterion("tjren <", value, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenLessThanOrEqualTo(Integer value) {
            addCriterion("tjren <=", value, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenIn(List<Integer> values) {
            addCriterion("tjren in", values, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenNotIn(List<Integer> values) {
            addCriterion("tjren not in", values, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenBetween(Integer value1, Integer value2) {
            addCriterion("tjren between", value1, value2, "tjren");
            return (Criteria) this;
        }

        public Criteria andTjrenNotBetween(Integer value1, Integer value2) {
            addCriterion("tjren not between", value1, value2, "tjren");
            return (Criteria) this;
        }

        public Criteria andRouteStatusIsNull() {
            addCriterion("route_status is null");
            return (Criteria) this;
        }

        public Criteria andRouteStatusIsNotNull() {
            addCriterion("route_status is not null");
            return (Criteria) this;
        }

        public Criteria andRouteStatusEqualTo(Integer value) {
            addCriterion("route_status =", value, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusNotEqualTo(Integer value) {
            addCriterion("route_status <>", value, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusGreaterThan(Integer value) {
            addCriterion("route_status >", value, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("route_status >=", value, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusLessThan(Integer value) {
            addCriterion("route_status <", value, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusLessThanOrEqualTo(Integer value) {
            addCriterion("route_status <=", value, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusIn(List<Integer> values) {
            addCriterion("route_status in", values, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusNotIn(List<Integer> values) {
            addCriterion("route_status not in", values, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusBetween(Integer value1, Integer value2) {
            addCriterion("route_status between", value1, value2, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRouteStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("route_status not between", value1, value2, "routeStatus");
            return (Criteria) this;
        }

        public Criteria andRobotNumberIsNull() {
            addCriterion("robot_number is null");
            return (Criteria) this;
        }

        public Criteria andRobotNumberIsNotNull() {
            addCriterion("robot_number is not null");
            return (Criteria) this;
        }

        public Criteria andRobotNumberEqualTo(Integer value) {
            addCriterion("robot_number =", value, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberNotEqualTo(Integer value) {
            addCriterion("robot_number <>", value, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberGreaterThan(Integer value) {
            addCriterion("robot_number >", value, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("robot_number >=", value, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberLessThan(Integer value) {
            addCriterion("robot_number <", value, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberLessThanOrEqualTo(Integer value) {
            addCriterion("robot_number <=", value, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberIn(List<Integer> values) {
            addCriterion("robot_number in", values, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberNotIn(List<Integer> values) {
            addCriterion("robot_number not in", values, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberBetween(Integer value1, Integer value2) {
            addCriterion("robot_number between", value1, value2, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("robot_number not between", value1, value2, "robotNumber");
            return (Criteria) this;
        }

        public Criteria andRobotidIsNull() {
            addCriterion("robotid is null");
            return (Criteria) this;
        }

        public Criteria andRobotidIsNotNull() {
            addCriterion("robotid is not null");
            return (Criteria) this;
        }

        public Criteria andRobotidEqualTo(Integer value) {
            addCriterion("robotid =", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotEqualTo(Integer value) {
            addCriterion("robotid <>", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidGreaterThan(Integer value) {
            addCriterion("robotid >", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidGreaterThanOrEqualTo(Integer value) {
            addCriterion("robotid >=", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidLessThan(Integer value) {
            addCriterion("robotid <", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidLessThanOrEqualTo(Integer value) {
            addCriterion("robotid <=", value, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidIn(List<Integer> values) {
            addCriterion("robotid in", values, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotIn(List<Integer> values) {
            addCriterion("robotid not in", values, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidBetween(Integer value1, Integer value2) {
            addCriterion("robotid between", value1, value2, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotidNotBetween(Integer value1, Integer value2) {
            addCriterion("robotid not between", value1, value2, "robotid");
            return (Criteria) this;
        }

        public Criteria andRobotRouteIsNull() {
            addCriterion("robot_route is null");
            return (Criteria) this;
        }

        public Criteria andRobotRouteIsNotNull() {
            addCriterion("robot_route is not null");
            return (Criteria) this;
        }

        public Criteria andRobotRouteEqualTo(String value) {
            addCriterion("robot_route =", value, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteNotEqualTo(String value) {
            addCriterion("robot_route <>", value, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteGreaterThan(String value) {
            addCriterion("robot_route >", value, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteGreaterThanOrEqualTo(String value) {
            addCriterion("robot_route >=", value, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteLessThan(String value) {
            addCriterion("robot_route <", value, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteLessThanOrEqualTo(String value) {
            addCriterion("robot_route <=", value, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteLike(String value) {
            addCriterion("robot_route like", value, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteNotLike(String value) {
            addCriterion("robot_route not like", value, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteIn(List<String> values) {
            addCriterion("robot_route in", values, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteNotIn(List<String> values) {
            addCriterion("robot_route not in", values, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteBetween(String value1, String value2) {
            addCriterion("robot_route between", value1, value2, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotRouteNotBetween(String value1, String value2) {
            addCriterion("robot_route not between", value1, value2, "robotRoute");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidIsNull() {
            addCriterion("robot_taskid is null");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidIsNotNull() {
            addCriterion("robot_taskid is not null");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidEqualTo(String value) {
            addCriterion("robot_taskid =", value, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidNotEqualTo(String value) {
            addCriterion("robot_taskid <>", value, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidGreaterThan(String value) {
            addCriterion("robot_taskid >", value, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidGreaterThanOrEqualTo(String value) {
            addCriterion("robot_taskid >=", value, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidLessThan(String value) {
            addCriterion("robot_taskid <", value, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidLessThanOrEqualTo(String value) {
            addCriterion("robot_taskid <=", value, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidLike(String value) {
            addCriterion("robot_taskid like", value, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidNotLike(String value) {
            addCriterion("robot_taskid not like", value, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidIn(List<String> values) {
            addCriterion("robot_taskid in", values, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidNotIn(List<String> values) {
            addCriterion("robot_taskid not in", values, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidBetween(String value1, String value2) {
            addCriterion("robot_taskid between", value1, value2, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotTaskidNotBetween(String value1, String value2) {
            addCriterion("robot_taskid not between", value1, value2, "robotTaskid");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayIsNull() {
            addCriterion("robot_gateway is null");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayIsNotNull() {
            addCriterion("robot_gateway is not null");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayEqualTo(String value) {
            addCriterion("robot_gateway =", value, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayNotEqualTo(String value) {
            addCriterion("robot_gateway <>", value, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayGreaterThan(String value) {
            addCriterion("robot_gateway >", value, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayGreaterThanOrEqualTo(String value) {
            addCriterion("robot_gateway >=", value, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayLessThan(String value) {
            addCriterion("robot_gateway <", value, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayLessThanOrEqualTo(String value) {
            addCriterion("robot_gateway <=", value, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayLike(String value) {
            addCriterion("robot_gateway like", value, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayNotLike(String value) {
            addCriterion("robot_gateway not like", value, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayIn(List<String> values) {
            addCriterion("robot_gateway in", values, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayNotIn(List<String> values) {
            addCriterion("robot_gateway not in", values, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayBetween(String value1, String value2) {
            addCriterion("robot_gateway between", value1, value2, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotGatewayNotBetween(String value1, String value2) {
            addCriterion("robot_gateway not between", value1, value2, "robotGateway");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberIsNull() {
            addCriterion("robot_displaynumber is null");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberIsNotNull() {
            addCriterion("robot_displaynumber is not null");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberEqualTo(String value) {
            addCriterion("robot_displaynumber =", value, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberNotEqualTo(String value) {
            addCriterion("robot_displaynumber <>", value, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberGreaterThan(String value) {
            addCriterion("robot_displaynumber >", value, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberGreaterThanOrEqualTo(String value) {
            addCriterion("robot_displaynumber >=", value, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberLessThan(String value) {
            addCriterion("robot_displaynumber <", value, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberLessThanOrEqualTo(String value) {
            addCriterion("robot_displaynumber <=", value, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberLike(String value) {
            addCriterion("robot_displaynumber like", value, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberNotLike(String value) {
            addCriterion("robot_displaynumber not like", value, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberIn(List<String> values) {
            addCriterion("robot_displaynumber in", values, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberNotIn(List<String> values) {
            addCriterion("robot_displaynumber not in", values, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberBetween(String value1, String value2) {
            addCriterion("robot_displaynumber between", value1, value2, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andRobotDisplaynumberNotBetween(String value1, String value2) {
            addCriterion("robot_displaynumber not between", value1, value2, "robotDisplaynumber");
            return (Criteria) this;
        }

        public Criteria andCallStatusIsNull() {
            addCriterion("call_status is null");
            return (Criteria) this;
        }

        public Criteria andCallStatusIsNotNull() {
            addCriterion("call_status is not null");
            return (Criteria) this;
        }

        public Criteria andCallStatusEqualTo(Integer value) {
            addCriterion("call_status =", value, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusNotEqualTo(Integer value) {
            addCriterion("call_status <>", value, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusGreaterThan(Integer value) {
            addCriterion("call_status >", value, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusGreaterThanOrEqualTo(Integer value) {
            addCriterion("call_status >=", value, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusLessThan(Integer value) {
            addCriterion("call_status <", value, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusLessThanOrEqualTo(Integer value) {
            addCriterion("call_status <=", value, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusIn(List<Integer> values) {
            addCriterion("call_status in", values, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusNotIn(List<Integer> values) {
            addCriterion("call_status not in", values, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusBetween(Integer value1, Integer value2) {
            addCriterion("call_status between", value1, value2, "callStatus");
            return (Criteria) this;
        }

        public Criteria andCallStatusNotBetween(Integer value1, Integer value2) {
            addCriterion("call_status not between", value1, value2, "callStatus");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceIsNull() {
            addCriterion("operator_huafei_balance is null");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceIsNotNull() {
            addCriterion("operator_huafei_balance is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceEqualTo(BigDecimal value) {
            addCriterion("operator_huafei_balance =", value, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceNotEqualTo(BigDecimal value) {
            addCriterion("operator_huafei_balance <>", value, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceGreaterThan(BigDecimal value) {
            addCriterion("operator_huafei_balance >", value, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("operator_huafei_balance >=", value, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceLessThan(BigDecimal value) {
            addCriterion("operator_huafei_balance <", value, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("operator_huafei_balance <=", value, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceIn(List<BigDecimal> values) {
            addCriterion("operator_huafei_balance in", values, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceNotIn(List<BigDecimal> values) {
            addCriterion("operator_huafei_balance not in", values, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("operator_huafei_balance between", value1, value2, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andOperatorHuafeiBalanceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("operator_huafei_balance not between", value1, value2, "operatorHuafeiBalance");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeIsNull() {
            addCriterion("decrease_type is null");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeIsNotNull() {
            addCriterion("decrease_type is not null");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeEqualTo(Integer value) {
            addCriterion("decrease_type =", value, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeNotEqualTo(Integer value) {
            addCriterion("decrease_type <>", value, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeGreaterThan(Integer value) {
            addCriterion("decrease_type >", value, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("decrease_type >=", value, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeLessThan(Integer value) {
            addCriterion("decrease_type <", value, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeLessThanOrEqualTo(Integer value) {
            addCriterion("decrease_type <=", value, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeIn(List<Integer> values) {
            addCriterion("decrease_type in", values, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeNotIn(List<Integer> values) {
            addCriterion("decrease_type not in", values, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeBetween(Integer value1, Integer value2) {
            addCriterion("decrease_type between", value1, value2, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("decrease_type not between", value1, value2, "decreaseType");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeIsNull() {
            addCriterion("decrease_minite_fee is null");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeIsNotNull() {
            addCriterion("decrease_minite_fee is not null");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeEqualTo(BigDecimal value) {
            addCriterion("decrease_minite_fee =", value, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeNotEqualTo(BigDecimal value) {
            addCriterion("decrease_minite_fee <>", value, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeGreaterThan(BigDecimal value) {
            addCriterion("decrease_minite_fee >", value, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("decrease_minite_fee >=", value, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeLessThan(BigDecimal value) {
            addCriterion("decrease_minite_fee <", value, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("decrease_minite_fee <=", value, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeIn(List<BigDecimal> values) {
            addCriterion("decrease_minite_fee in", values, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeNotIn(List<BigDecimal> values) {
            addCriterion("decrease_minite_fee not in", values, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("decrease_minite_fee between", value1, value2, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andDecreaseMiniteFeeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("decrease_minite_fee not between", value1, value2, "decreaseMiniteFee");
            return (Criteria) this;
        }

        public Criteria andOverspendIsNull() {
            addCriterion("overspend is null");
            return (Criteria) this;
        }

        public Criteria andOverspendIsNotNull() {
            addCriterion("overspend is not null");
            return (Criteria) this;
        }

        public Criteria andOverspendEqualTo(BigDecimal value) {
            addCriterion("overspend =", value, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendNotEqualTo(BigDecimal value) {
            addCriterion("overspend <>", value, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendGreaterThan(BigDecimal value) {
            addCriterion("overspend >", value, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("overspend >=", value, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendLessThan(BigDecimal value) {
            addCriterion("overspend <", value, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendLessThanOrEqualTo(BigDecimal value) {
            addCriterion("overspend <=", value, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendIn(List<BigDecimal> values) {
            addCriterion("overspend in", values, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendNotIn(List<BigDecimal> values) {
            addCriterion("overspend not in", values, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("overspend between", value1, value2, "overspend");
            return (Criteria) this;
        }

        public Criteria andOverspendNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("overspend not between", value1, value2, "overspend");
            return (Criteria) this;
        }

        public Criteria andShareIsNull() {
            addCriterion("share is null");
            return (Criteria) this;
        }

        public Criteria andShareIsNotNull() {
            addCriterion("share is not null");
            return (Criteria) this;
        }

        public Criteria andShareEqualTo(Byte value) {
            addCriterion("share =", value, "share");
            return (Criteria) this;
        }

        public Criteria andShareNotEqualTo(Byte value) {
            addCriterion("share <>", value, "share");
            return (Criteria) this;
        }

        public Criteria andShareGreaterThan(Byte value) {
            addCriterion("share >", value, "share");
            return (Criteria) this;
        }

        public Criteria andShareGreaterThanOrEqualTo(Byte value) {
            addCriterion("share >=", value, "share");
            return (Criteria) this;
        }

        public Criteria andShareLessThan(Byte value) {
            addCriterion("share <", value, "share");
            return (Criteria) this;
        }

        public Criteria andShareLessThanOrEqualTo(Byte value) {
            addCriterion("share <=", value, "share");
            return (Criteria) this;
        }

        public Criteria andShareIn(List<Byte> values) {
            addCriterion("share in", values, "share");
            return (Criteria) this;
        }

        public Criteria andShareNotIn(List<Byte> values) {
            addCriterion("share not in", values, "share");
            return (Criteria) this;
        }

        public Criteria andShareBetween(Byte value1, Byte value2) {
            addCriterion("share between", value1, value2, "share");
            return (Criteria) this;
        }

        public Criteria andShareNotBetween(Byte value1, Byte value2) {
            addCriterion("share not between", value1, value2, "share");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneIsNull() {
            addCriterion("isshowphone is null");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneIsNotNull() {
            addCriterion("isshowphone is not null");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneEqualTo(Byte value) {
            addCriterion("isshowphone =", value, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneNotEqualTo(Byte value) {
            addCriterion("isshowphone <>", value, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneGreaterThan(Byte value) {
            addCriterion("isshowphone >", value, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneGreaterThanOrEqualTo(Byte value) {
            addCriterion("isshowphone >=", value, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneLessThan(Byte value) {
            addCriterion("isshowphone <", value, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneLessThanOrEqualTo(Byte value) {
            addCriterion("isshowphone <=", value, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneIn(List<Byte> values) {
            addCriterion("isshowphone in", values, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneNotIn(List<Byte> values) {
            addCriterion("isshowphone not in", values, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneBetween(Byte value1, Byte value2) {
            addCriterion("isshowphone between", value1, value2, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andIsshowphoneNotBetween(Byte value1, Byte value2) {
            addCriterion("isshowphone not between", value1, value2, "isshowphone");
            return (Criteria) this;
        }

        public Criteria andMemberNoIsNull() {
            addCriterion("member_no is null");
            return (Criteria) this;
        }

        public Criteria andMemberNoIsNotNull() {
            addCriterion("member_no is not null");
            return (Criteria) this;
        }

        public Criteria andMemberNoEqualTo(String value) {
            addCriterion("member_no =", value, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoNotEqualTo(String value) {
            addCriterion("member_no <>", value, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoGreaterThan(String value) {
            addCriterion("member_no >", value, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoGreaterThanOrEqualTo(String value) {
            addCriterion("member_no >=", value, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoLessThan(String value) {
            addCriterion("member_no <", value, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoLessThanOrEqualTo(String value) {
            addCriterion("member_no <=", value, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoLike(String value) {
            addCriterion("member_no like", value, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoNotLike(String value) {
            addCriterion("member_no not like", value, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoIn(List<String> values) {
            addCriterion("member_no in", values, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoNotIn(List<String> values) {
            addCriterion("member_no not in", values, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoBetween(String value1, String value2) {
            addCriterion("member_no between", value1, value2, "memberNo");
            return (Criteria) this;
        }

        public Criteria andMemberNoNotBetween(String value1, String value2) {
            addCriterion("member_no not between", value1, value2, "memberNo");
            return (Criteria) this;
        }

        public Criteria andIsSystemIsNull() {
            addCriterion("is_system is null");
            return (Criteria) this;
        }

        public Criteria andIsSystemIsNotNull() {
            addCriterion("is_system is not null");
            return (Criteria) this;
        }

        public Criteria andIsSystemEqualTo(Integer value) {
            addCriterion("is_system =", value, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemNotEqualTo(Integer value) {
            addCriterion("is_system <>", value, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemGreaterThan(Integer value) {
            addCriterion("is_system >", value, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_system >=", value, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemLessThan(Integer value) {
            addCriterion("is_system <", value, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemLessThanOrEqualTo(Integer value) {
            addCriterion("is_system <=", value, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemIn(List<Integer> values) {
            addCriterion("is_system in", values, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemNotIn(List<Integer> values) {
            addCriterion("is_system not in", values, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemBetween(Integer value1, Integer value2) {
            addCriterion("is_system between", value1, value2, "isSystem");
            return (Criteria) this;
        }

        public Criteria andIsSystemNotBetween(Integer value1, Integer value2) {
            addCriterion("is_system not between", value1, value2, "isSystem");
            return (Criteria) this;
        }

        public Criteria andBigdataCountIsNull() {
            addCriterion("bigdata_count is null");
            return (Criteria) this;
        }

        public Criteria andBigdataCountIsNotNull() {
            addCriterion("bigdata_count is not null");
            return (Criteria) this;
        }

        public Criteria andBigdataCountEqualTo(BigDecimal value) {
            addCriterion("bigdata_count =", value, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountNotEqualTo(BigDecimal value) {
            addCriterion("bigdata_count <>", value, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountGreaterThan(BigDecimal value) {
            addCriterion("bigdata_count >", value, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("bigdata_count >=", value, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountLessThan(BigDecimal value) {
            addCriterion("bigdata_count <", value, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("bigdata_count <=", value, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountIn(List<BigDecimal> values) {
            addCriterion("bigdata_count in", values, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountNotIn(List<BigDecimal> values) {
            addCriterion("bigdata_count not in", values, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bigdata_count between", value1, value2, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andBigdataCountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("bigdata_count not between", value1, value2, "bigdataCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountIsNull() {
            addCriterion("translate_count is null");
            return (Criteria) this;
        }

        public Criteria andTranslateCountIsNotNull() {
            addCriterion("translate_count is not null");
            return (Criteria) this;
        }

        public Criteria andTranslateCountEqualTo(Integer value) {
            addCriterion("translate_count =", value, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountNotEqualTo(Integer value) {
            addCriterion("translate_count <>", value, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountGreaterThan(Integer value) {
            addCriterion("translate_count >", value, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("translate_count >=", value, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountLessThan(Integer value) {
            addCriterion("translate_count <", value, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountLessThanOrEqualTo(Integer value) {
            addCriterion("translate_count <=", value, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountIn(List<Integer> values) {
            addCriterion("translate_count in", values, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountNotIn(List<Integer> values) {
            addCriterion("translate_count not in", values, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountBetween(Integer value1, Integer value2) {
            addCriterion("translate_count between", value1, value2, "translateCount");
            return (Criteria) this;
        }

        public Criteria andTranslateCountNotBetween(Integer value1, Integer value2) {
            addCriterion("translate_count not between", value1, value2, "translateCount");
            return (Criteria) this;
        }

        public Criteria andIsTranslateIsNull() {
            addCriterion("is_translate is null");
            return (Criteria) this;
        }

        public Criteria andIsTranslateIsNotNull() {
            addCriterion("is_translate is not null");
            return (Criteria) this;
        }

        public Criteria andIsTranslateEqualTo(Integer value) {
            addCriterion("is_translate =", value, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateNotEqualTo(Integer value) {
            addCriterion("is_translate <>", value, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateGreaterThan(Integer value) {
            addCriterion("is_translate >", value, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_translate >=", value, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateLessThan(Integer value) {
            addCriterion("is_translate <", value, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateLessThanOrEqualTo(Integer value) {
            addCriterion("is_translate <=", value, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateIn(List<Integer> values) {
            addCriterion("is_translate in", values, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateNotIn(List<Integer> values) {
            addCriterion("is_translate not in", values, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateBetween(Integer value1, Integer value2) {
            addCriterion("is_translate between", value1, value2, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andIsTranslateNotBetween(Integer value1, Integer value2) {
            addCriterion("is_translate not between", value1, value2, "isTranslate");
            return (Criteria) this;
        }

        public Criteria andRobotTotalIsNull() {
            addCriterion("robot_total is null");
            return (Criteria) this;
        }

        public Criteria andRobotTotalIsNotNull() {
            addCriterion("robot_total is not null");
            return (Criteria) this;
        }

        public Criteria andRobotTotalEqualTo(Integer value) {
            addCriterion("robot_total =", value, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalNotEqualTo(Integer value) {
            addCriterion("robot_total <>", value, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalGreaterThan(Integer value) {
            addCriterion("robot_total >", value, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalGreaterThanOrEqualTo(Integer value) {
            addCriterion("robot_total >=", value, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalLessThan(Integer value) {
            addCriterion("robot_total <", value, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalLessThanOrEqualTo(Integer value) {
            addCriterion("robot_total <=", value, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalIn(List<Integer> values) {
            addCriterion("robot_total in", values, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalNotIn(List<Integer> values) {
            addCriterion("robot_total not in", values, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalBetween(Integer value1, Integer value2) {
            addCriterion("robot_total between", value1, value2, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andRobotTotalNotBetween(Integer value1, Integer value2) {
            addCriterion("robot_total not between", value1, value2, "robotTotal");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateIsNull() {
            addCriterion("is_edit_template is null");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateIsNotNull() {
            addCriterion("is_edit_template is not null");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateEqualTo(Integer value) {
            addCriterion("is_edit_template =", value, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateNotEqualTo(Integer value) {
            addCriterion("is_edit_template <>", value, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateGreaterThan(Integer value) {
            addCriterion("is_edit_template >", value, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_edit_template >=", value, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateLessThan(Integer value) {
            addCriterion("is_edit_template <", value, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateLessThanOrEqualTo(Integer value) {
            addCriterion("is_edit_template <=", value, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateIn(List<Integer> values) {
            addCriterion("is_edit_template in", values, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateNotIn(List<Integer> values) {
            addCriterion("is_edit_template not in", values, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateBetween(Integer value1, Integer value2) {
            addCriterion("is_edit_template between", value1, value2, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsEditTemplateNotBetween(Integer value1, Integer value2) {
            addCriterion("is_edit_template not between", value1, value2, "isEditTemplate");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallIsNull() {
            addCriterion("is_limit_call is null");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallIsNotNull() {
            addCriterion("is_limit_call is not null");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallEqualTo(Integer value) {
            addCriterion("is_limit_call =", value, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallNotEqualTo(Integer value) {
            addCriterion("is_limit_call <>", value, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallGreaterThan(Integer value) {
            addCriterion("is_limit_call >", value, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_limit_call >=", value, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallLessThan(Integer value) {
            addCriterion("is_limit_call <", value, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallLessThanOrEqualTo(Integer value) {
            addCriterion("is_limit_call <=", value, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallIn(List<Integer> values) {
            addCriterion("is_limit_call in", values, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallNotIn(List<Integer> values) {
            addCriterion("is_limit_call not in", values, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallBetween(Integer value1, Integer value2) {
            addCriterion("is_limit_call between", value1, value2, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsLimitCallNotBetween(Integer value1, Integer value2) {
            addCriterion("is_limit_call not between", value1, value2, "isLimitCall");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightIsNull() {
            addCriterion("is_copyright is null");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightIsNotNull() {
            addCriterion("is_copyright is not null");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightEqualTo(Integer value) {
            addCriterion("is_copyright =", value, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightNotEqualTo(Integer value) {
            addCriterion("is_copyright <>", value, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightGreaterThan(Integer value) {
            addCriterion("is_copyright >", value, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightGreaterThanOrEqualTo(Integer value) {
            addCriterion("is_copyright >=", value, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightLessThan(Integer value) {
            addCriterion("is_copyright <", value, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightLessThanOrEqualTo(Integer value) {
            addCriterion("is_copyright <=", value, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightIn(List<Integer> values) {
            addCriterion("is_copyright in", values, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightNotIn(List<Integer> values) {
            addCriterion("is_copyright not in", values, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightBetween(Integer value1, Integer value2) {
            addCriterion("is_copyright between", value1, value2, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andIsCopyrightNotBetween(Integer value1, Integer value2) {
            addCriterion("is_copyright not between", value1, value2, "isCopyright");
            return (Criteria) this;
        }

        public Criteria andCustomIsNull() {
            addCriterion("custom is null");
            return (Criteria) this;
        }

        public Criteria andCustomIsNotNull() {
            addCriterion("custom is not null");
            return (Criteria) this;
        }

        public Criteria andCustomEqualTo(String value) {
            addCriterion("custom =", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomNotEqualTo(String value) {
            addCriterion("custom <>", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomGreaterThan(String value) {
            addCriterion("custom >", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomGreaterThanOrEqualTo(String value) {
            addCriterion("custom >=", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomLessThan(String value) {
            addCriterion("custom <", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomLessThanOrEqualTo(String value) {
            addCriterion("custom <=", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomLike(String value) {
            addCriterion("custom like", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomNotLike(String value) {
            addCriterion("custom not like", value, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomIn(List<String> values) {
            addCriterion("custom in", values, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomNotIn(List<String> values) {
            addCriterion("custom not in", values, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomBetween(String value1, String value2) {
            addCriterion("custom between", value1, value2, "custom");
            return (Criteria) this;
        }

        public Criteria andCustomNotBetween(String value1, String value2) {
            addCriterion("custom not between", value1, value2, "custom");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberIsNull() {
            addCriterion("use_robot_number is null");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberIsNotNull() {
            addCriterion("use_robot_number is not null");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberEqualTo(Integer value) {
            addCriterion("use_robot_number =", value, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberNotEqualTo(Integer value) {
            addCriterion("use_robot_number <>", value, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberGreaterThan(Integer value) {
            addCriterion("use_robot_number >", value, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberGreaterThanOrEqualTo(Integer value) {
            addCriterion("use_robot_number >=", value, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberLessThan(Integer value) {
            addCriterion("use_robot_number <", value, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberLessThanOrEqualTo(Integer value) {
            addCriterion("use_robot_number <=", value, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberIn(List<Integer> values) {
            addCriterion("use_robot_number in", values, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberNotIn(List<Integer> values) {
            addCriterion("use_robot_number not in", values, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberBetween(Integer value1, Integer value2) {
            addCriterion("use_robot_number between", value1, value2, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andUseRobotNumberNotBetween(Integer value1, Integer value2) {
            addCriterion("use_robot_number not between", value1, value2, "useRobotNumber");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountIsNull() {
            addCriterion("today_translate_count is null");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountIsNotNull() {
            addCriterion("today_translate_count is not null");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountEqualTo(Integer value) {
            addCriterion("today_translate_count =", value, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountNotEqualTo(Integer value) {
            addCriterion("today_translate_count <>", value, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountGreaterThan(Integer value) {
            addCriterion("today_translate_count >", value, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountGreaterThanOrEqualTo(Integer value) {
            addCriterion("today_translate_count >=", value, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountLessThan(Integer value) {
            addCriterion("today_translate_count <", value, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountLessThanOrEqualTo(Integer value) {
            addCriterion("today_translate_count <=", value, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountIn(List<Integer> values) {
            addCriterion("today_translate_count in", values, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountNotIn(List<Integer> values) {
            addCriterion("today_translate_count not in", values, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountBetween(Integer value1, Integer value2) {
            addCriterion("today_translate_count between", value1, value2, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andTodayTranslateCountNotBetween(Integer value1, Integer value2) {
            addCriterion("today_translate_count not between", value1, value2, "todayTranslateCount");
            return (Criteria) this;
        }

        public Criteria andLineIdIsNull() {
            addCriterion("line_id is null");
            return (Criteria) this;
        }

        public Criteria andLineIdIsNotNull() {
            addCriterion("line_id is not null");
            return (Criteria) this;
        }

        public Criteria andLineIdEqualTo(Integer value) {
            addCriterion("line_id =", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdNotEqualTo(Integer value) {
            addCriterion("line_id <>", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdGreaterThan(Integer value) {
            addCriterion("line_id >", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("line_id >=", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdLessThan(Integer value) {
            addCriterion("line_id <", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdLessThanOrEqualTo(Integer value) {
            addCriterion("line_id <=", value, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdIn(List<Integer> values) {
            addCriterion("line_id in", values, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdNotIn(List<Integer> values) {
            addCriterion("line_id not in", values, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdBetween(Integer value1, Integer value2) {
            addCriterion("line_id between", value1, value2, "lineId");
            return (Criteria) this;
        }

        public Criteria andLineIdNotBetween(Integer value1, Integer value2) {
            addCriterion("line_id not between", value1, value2, "lineId");
            return (Criteria) this;
        }

        public Criteria andIfOursIsNull() {
            addCriterion("if_ours is null");
            return (Criteria) this;
        }

        public Criteria andIfOursIsNotNull() {
            addCriterion("if_ours is not null");
            return (Criteria) this;
        }

        public Criteria andIfOursEqualTo(Boolean value) {
            addCriterion("if_ours =", value, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursNotEqualTo(Boolean value) {
            addCriterion("if_ours <>", value, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursGreaterThan(Boolean value) {
            addCriterion("if_ours >", value, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursGreaterThanOrEqualTo(Boolean value) {
            addCriterion("if_ours >=", value, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursLessThan(Boolean value) {
            addCriterion("if_ours <", value, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursLessThanOrEqualTo(Boolean value) {
            addCriterion("if_ours <=", value, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursIn(List<Boolean> values) {
            addCriterion("if_ours in", values, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursNotIn(List<Boolean> values) {
            addCriterion("if_ours not in", values, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursBetween(Boolean value1, Boolean value2) {
            addCriterion("if_ours between", value1, value2, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andIfOursNotBetween(Boolean value1, Boolean value2) {
            addCriterion("if_ours not between", value1, value2, "ifOurs");
            return (Criteria) this;
        }

        public Criteria andLimitCallIsNull() {
            addCriterion("limit_call is null");
            return (Criteria) this;
        }

        public Criteria andLimitCallIsNotNull() {
            addCriterion("limit_call is not null");
            return (Criteria) this;
        }

        public Criteria andLimitCallEqualTo(Integer value) {
            addCriterion("limit_call =", value, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallNotEqualTo(Integer value) {
            addCriterion("limit_call <>", value, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallGreaterThan(Integer value) {
            addCriterion("limit_call >", value, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallGreaterThanOrEqualTo(Integer value) {
            addCriterion("limit_call >=", value, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallLessThan(Integer value) {
            addCriterion("limit_call <", value, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallLessThanOrEqualTo(Integer value) {
            addCriterion("limit_call <=", value, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallIn(List<Integer> values) {
            addCriterion("limit_call in", values, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallNotIn(List<Integer> values) {
            addCriterion("limit_call not in", values, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallBetween(Integer value1, Integer value2) {
            addCriterion("limit_call between", value1, value2, "limitCall");
            return (Criteria) this;
        }

        public Criteria andLimitCallNotBetween(Integer value1, Integer value2) {
            addCriterion("limit_call not between", value1, value2, "limitCall");
            return (Criteria) this;
        }

        public Criteria andIsSectionIsNull() {
            addCriterion("is_section is null");
            return (Criteria) this;
        }

        public Criteria andIsSectionIsNotNull() {
            addCriterion("is_section is not null");
            return (Criteria) this;
        }

        public Criteria andIsSectionEqualTo(Boolean value) {
            addCriterion("is_section =", value, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionNotEqualTo(Boolean value) {
            addCriterion("is_section <>", value, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionGreaterThan(Boolean value) {
            addCriterion("is_section >", value, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionGreaterThanOrEqualTo(Boolean value) {
            addCriterion("is_section >=", value, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionLessThan(Boolean value) {
            addCriterion("is_section <", value, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionLessThanOrEqualTo(Boolean value) {
            addCriterion("is_section <=", value, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionIn(List<Boolean> values) {
            addCriterion("is_section in", values, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionNotIn(List<Boolean> values) {
            addCriterion("is_section not in", values, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionBetween(Boolean value1, Boolean value2) {
            addCriterion("is_section between", value1, value2, "isSection");
            return (Criteria) this;
        }

        public Criteria andIsSectionNotBetween(Boolean value1, Boolean value2) {
            addCriterion("is_section not between", value1, value2, "isSection");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}