package com.example.demo.dao.slaver;

import com.example.demo.pojo.ImsMembersPaylog;
import com.example.demo.pojo.ImsMembersPaylogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ImsMembersPaylogMapper {
    int countByExample(ImsMembersPaylogExample example);

    int deleteByExample(ImsMembersPaylogExample example);

    int insert(ImsMembersPaylog record);

    int insertSelective(ImsMembersPaylog record);

    List<ImsMembersPaylog> selectByExample(ImsMembersPaylogExample example);

    int updateByExampleSelective(@Param("record") ImsMembersPaylog record, @Param("example") ImsMembersPaylogExample example);

    int updateByExample(@Param("record") ImsMembersPaylog record, @Param("example") ImsMembersPaylogExample example);
}