package com.example.demo.dao.slaver;

import com.example.demo.pojo.ImsMembers;
import com.example.demo.pojo.ImsMembersExample;
import com.example.demo.pojo.ImsMembersWithBLOBs;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ImsMembersMapper {
    int countByExample(ImsMembersExample example);

    int deleteByExample(ImsMembersExample example);

    int deleteByPrimaryKey(Integer uid);

    int insert(ImsMembersWithBLOBs record);

    int insertSelective(ImsMembersWithBLOBs record);

    List<ImsMembersWithBLOBs> selectByExampleWithBLOBs(ImsMembersExample example);

    List<ImsMembers> selectByExample(ImsMembersExample example);

    ImsMembersWithBLOBs selectByPrimaryKey(Integer uid);

    int updateByExampleSelective(@Param("record") ImsMembersWithBLOBs record, @Param("example") ImsMembersExample example);

    int updateByExampleWithBLOBs(@Param("record") ImsMembersWithBLOBs record, @Param("example") ImsMembersExample example);

    int updateByExample(@Param("record") ImsMembers record, @Param("example") ImsMembersExample example);

    int updateByPrimaryKeySelective(ImsMembersWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(ImsMembersWithBLOBs record);

    int updateByUserName(ImsMembers record);

	ImsMembers selectByUserName(String userName);
}