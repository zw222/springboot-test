package com.example.demo.dao.slaver;

import com.example.demo.pojo.WdfPayLoger;
import com.example.demo.pojo.WdfPayLogerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WdfPayLogerMapper {
    int countByExample(WdfPayLogerExample example);

    int deleteByExample(WdfPayLogerExample example);

    int deleteByPrimaryKey(Long id);

    int insert(WdfPayLoger record);

    int insertSelective(WdfPayLoger record);

    List<WdfPayLoger> selectByExample(WdfPayLogerExample example);

    WdfPayLoger selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") WdfPayLoger record, @Param("example") WdfPayLogerExample example);

    int updateByExample(@Param("record") WdfPayLoger record, @Param("example") WdfPayLogerExample example);

    int updateByPrimaryKeySelective(WdfPayLoger record);

    int updateByPrimaryOrderNum(WdfPayLoger record);

	WdfPayLoger selectByOutTradeNo(String outTradeNo);
}