package com.example.demo.dao.master;

import com.example.demo.pojo.WdfAdminUser;
import com.example.demo.pojo.WdfAdminUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WdfAdminUserMapper {
    int countByExample(WdfAdminUserExample example);

    int deleteByExample(WdfAdminUserExample example);

    int insert(WdfAdminUser record);

    int insertSelective(WdfAdminUser record);

    List<WdfAdminUser> selectByExample(WdfAdminUserExample example);

    int updateByExampleSelective(@Param("record") WdfAdminUser record, @Param("example") WdfAdminUserExample example);

    int updateByExample(@Param("record") WdfAdminUser record, @Param("example") WdfAdminUserExample example);

	WdfAdminUser selectByName(String name);
}