package com.example.demo.dao.master;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.example.demo.pojo.Phone;

/**
 * 
 * PhoneMapper数据库操作接口类
 * 
 **/

public interface PhoneMapper{


	/**
	 * 
	 * 查询（根据主键ID查询）
	 * 
	 **/
	Phone  selectByPrimaryKey ( @Param("id") Long id );

	/**
	 * 
	 * 删除（根据主键ID删除）
	 * 
	 **/
	int deleteByPrimaryKey ( @Param("id") Long id );

	/**
	 * 
	 * 添加
	 * 
	 **/
	int insert( Phone record );

	/**
	 * 
	 * 添加 （匹配有值的字段）
	 * 
	 **/
	int insertSelective( Phone record );

	/**
	 * 
	 * 修改 （匹配有值的字段）
	 * 
	 **/
	int updateByPrimaryKeySelective( Phone record );

	/**
	 * 
	 * 修改（根据主键ID修改）
	 * 
	 **/
	int updateByPrimaryKey ( Phone record );

	int getAllTotalPhone(Map<String, Object> map);

	List<Phone> getPhoneList(Map<String, Object> map);


}