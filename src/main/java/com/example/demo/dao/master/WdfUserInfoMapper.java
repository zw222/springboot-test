package com.example.demo.dao.master;

import com.example.demo.pojo.WdfUserInfo;
import com.example.demo.pojo.WdfUserInfoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface WdfUserInfoMapper {
    int countByExample(WdfUserInfoExample example);

    int deleteByExample(WdfUserInfoExample example);

    int insert(WdfUserInfo record);

    int insertSelective(WdfUserInfo record);

    List<WdfUserInfo> selectByExample(WdfUserInfoExample example);

    int updateByExampleSelective(@Param("record") WdfUserInfo record, @Param("example") WdfUserInfoExample example);

    int updateByExample(@Param("record") WdfUserInfo record, @Param("example") WdfUserInfoExample example);
}