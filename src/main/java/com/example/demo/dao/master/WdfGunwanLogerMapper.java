package com.example.demo.dao.master;

import com.example.demo.pojo.WdfGunwanLoger;
import com.example.demo.pojo.WdfGunwanLogerExample;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

public interface WdfGunwanLogerMapper {
	
	 List<Map<String, Object>> selectEveryDayNum();
	 
    int countByExample(WdfGunwanLogerExample example);

    int deleteByExample(WdfGunwanLogerExample example);

    int deleteByPrimaryKey(String id);

    int insert(WdfGunwanLoger record);

    int insertSelective(WdfGunwanLoger record);

    List<WdfGunwanLoger> selectByExample(WdfGunwanLogerExample example);

    WdfGunwanLoger selectByPrimaryKey(String id);
    
    int updateByExampleSelective(@Param("record") WdfGunwanLoger record, @Param("example") WdfGunwanLogerExample example);

    int updateByExample(@Param("record") WdfGunwanLoger record, @Param("example") WdfGunwanLogerExample example);

    int updateByPrimaryKeySelective(WdfGunwanLoger record);

    int updateByPrimaryKey(WdfGunwanLoger record);
}