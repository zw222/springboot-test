package com.example.demo.service;

import java.text.ParseException;
import java.util.HashMap;

import com.example.demo.pojo.WdfUserInfo;

public interface WdfUserInfoService {
	
	public int insert(WdfUserInfo record);

	HashMap<String, Object> selectUserInfoList(int pageNumber, int pageSize, String StartTime, String endTime)
			throws ParseException;
}
