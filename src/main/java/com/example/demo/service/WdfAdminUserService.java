package com.example.demo.service;

import com.example.demo.pojo.WdfAdminUser;

public interface WdfAdminUserService {

	WdfAdminUser selectByName(String name);

}
