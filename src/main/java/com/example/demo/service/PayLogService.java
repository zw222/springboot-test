package com.example.demo.service;

import java.util.Map;

import com.example.demo.pojo.WdfPayLoger;
import com.example.demo.utils.JsonResult;

public interface PayLogService {
	/**
	 * 支付日志
	 */
	JsonResult insert(Map<String,String> map);
	JsonResult updateState(WdfPayLoger payLoger);

}
