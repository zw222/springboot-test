package com.example.demo.service;

import com.example.demo.pojo.ImsMembers;

public interface ImsMembersService {

	ImsMembers selectByUserName(String userName);

	int upMemberMoney(ImsMembers record);

}
