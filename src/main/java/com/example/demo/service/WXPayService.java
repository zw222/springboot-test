package com.example.demo.service;

import java.util.Map;

public interface WXPayService {

	public Map<String,String> weixinPay(String userId, String totalFee) throws Exception;
	
	public Object closeOrder(String out_trade_no);
}
