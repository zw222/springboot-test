package com.example.demo.service;

import java.text.ParseException;
import java.util.HashMap;

import com.example.demo.pojo.WdfGunwanLoger;

/**
 * 官网浏览日志
 * @author Hasee
 *
 */
public interface GunwanLogerService {
	
	public HashMap<String, Object> getGuanWanLogList(int pageNumber ,int pageSize ,String StartTime, String endTime ) throws ParseException;
	
	public Integer insert(WdfGunwanLoger  record);
	
	public  Object selectEveryDayNum();
	
	
}
