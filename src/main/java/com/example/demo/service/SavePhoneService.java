package com.example.demo.service;

import java.util.Map;

import com.example.demo.pojo.Phone;

public interface SavePhoneService {

	public Integer Savephone(String phoneNumber);

	public Map<String, Object> getPhoneList(int start, int length, Phone phone);
}
