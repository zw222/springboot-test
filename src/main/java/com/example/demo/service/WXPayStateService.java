package com.example.demo.service;

import java.util.Map;

public interface WXPayStateService {
	/**
	 * 查询支付状态
	 * @param out_trade_no
	 * @return
	 */
	public  Map<String,String>  getWxPayState(String out_trade_no);
}
