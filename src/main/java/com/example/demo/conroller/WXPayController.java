package com.example.demo.conroller;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.slaver.ImsMembersMapper;
import com.example.demo.pojo.ImsMembers;
import com.example.demo.pojo.WdfPayLoger;
import com.example.demo.service.ImsMembersPaylogService;
import com.example.demo.service.PayLogService;
import com.example.demo.service.WXPayService;
import com.example.demo.service.WXPayStateService;
import com.example.demo.utils.JsonResult;
import com.example.demo.utils.ResultConst;

/**
 * wx
 * 
 * @author Hasee
 *
 */
@RestController
public class WXPayController {

	@Autowired
	public WXPayService wXPayService;

	@Autowired
	public WXPayStateService wXPayStateService;

	@Autowired
	public PayLogService payLogService;

	@Autowired
	public ImsMembersMapper imsMembersMapper;
	
	@Autowired
	public ImsMembersPaylogService imsMembersPaylogService;

	/**
	 * wx下单
	 * 
	 * @param userName
	 * @param totalFee
	 * @return
	 */
	@CrossOrigin
	@RequestMapping("wXPay")
	public JsonResult wXPay(String userName, String totalFee) {
		Map<String, String> weixinPay = null;
		try {
			weixinPay = wXPayService.weixinPay(userName, totalFee);
			if (weixinPay == null || StringUtils.isBlank(weixinPay.get("url"))) {
				return new JsonResult(ResultConst.FALSE_STATE, false, "请求失败");
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonResult(ResultConst.FALSE_STATE, false, "请求失败");
		}
		return new JsonResult(ResultConst.SUCCESS_STATE, true, "请求成功", weixinPay);
	}

	/**
	 * 支付状态
	 * 
	 * @param outTradeNo
	 * 
	 * @return
	 */
	@CrossOrigin
	@RequestMapping("getWxPayState")
	public JsonResult getWxPayState(String outTradeNo, String userName) {
		if (StringUtils.isBlank(userName)) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请输入用户名");
		}
		if (StringUtils.isBlank(outTradeNo)) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请输入订单号");
		}

		int x = 0;
		while (true) {
			Map<String, String> map = wXPayStateService.getWxPayState(outTradeNo);
			if (map == null) {
				return new JsonResult(ResultConst.FALSE_STATE, false, "系统错误");

			}
			if (map.get("result_code").equals("FAIL")) {
				return new JsonResult(ResultConst.FALSE_STATE, false, "系统错误或订单不存在");
			}
			if (map.get("trade_state").equals("SUCCESS")) {
				// 跟新日志状态
				WdfPayLoger loger = new WdfPayLoger();
				loger.setState((byte) 2);
				loger.setPayTime(new Date());
				loger.setOrderNumber(outTradeNo);
				payLogService.updateState(loger);
				//机器人日志
				imsMembersPaylogService.insert(userName, outTradeNo);
				ImsMembers imsMembers = new ImsMembers();
				imsMembers.setUsername(userName);
				imsMembers.setMoney(new BigDecimal(map.get("cash_fee")).divide(new BigDecimal(100)));
				imsMembersMapper.updateByUserName(imsMembers);
				return new JsonResult(ResultConst.SUCCESS_STATE, true, "支付成功");
			}

			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return new JsonResult(ResultConst.FALSE_STATE, false, "系统错误");
			}
			x++;
			if (x > 100) {
				wXPayService.closeOrder(outTradeNo);
				return new JsonResult(ResultConst.FALSE_STATE, false, "二维码超时");
			}
		}
	}

}
