package com.example.demo.conroller;

import java.text.ParseException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.pojo.WdfGunwanLoger;
import com.example.demo.service.GunwanLogerService;
import com.example.demo.utils.IPUtils;
import com.example.demo.utils.JsonResult;
import com.example.demo.utils.ResultConst;

@RestController
@RequestMapping("gwLog")
public class GunwanLogerController {

	@Autowired
	private GunwanLogerService gunwanLogerService;

	/**
	 * 保存官网日志
	 * 
	 * @param record
	 */
	@RequestMapping("insertLog")
	@CrossOrigin
	public void insertLog(WdfGunwanLoger record, HttpServletRequest request) {
		try {
			record.setIp(IPUtils.getIpAddr(request));
			gunwanLogerService.insert(record);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 官网日志列表
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param StartTime
	 * @param endTime
	 * @return
	 */
	@RequestMapping("getGuanWanLogList")
	@CrossOrigin
	public Object getGuanWanLogList(Integer pageNumber, Integer pageSize,
			@RequestParam(value = "StartTime", required = false) String StartTime,
			@RequestParam(value = "endTime", required = false) String endTime) {
		HashMap<String, Object> List = null;
		try {
			List = gunwanLogerService.getGuanWanLogList(pageNumber, pageSize, StartTime, endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return List;
	}

	/**
	 * 单月访问量
	 * 
	 * @return
	 */
	@RequestMapping("selectMonthEveryDayNum")
	@CrossOrigin
	public Object selectMonthEveryDayNum() {
		Object selectEveryDayNum=null;
		try {
			selectEveryDayNum = gunwanLogerService.selectEveryDayNum();
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonResult(ResultConst.FALSE_STATE, false, "请求失败");
		}
		return new JsonResult(ResultConst.SUCCESS_STATE, true, "请求成功", selectEveryDayNum);
	}
}
