package com.example.demo.conroller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.pojo.WdfPayLoger;
import com.example.demo.utils.JsonResult;
import com.example.demo.utils.ResultConst;

@RestController
public class PayLogController {
	/**
	 * 支付日志
	 * @return 
	 */
	@RequestMapping("payLog")
	public JsonResult savePayLog(WdfPayLoger record) {
		if(record==null) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请传递参数");
		}
		if(StringUtils.isNoneBlank(record.getUserName())) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请输入用户名");
		}
		if(StringUtils.isNoneBlank(record.getOrderNumber())) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请输入订单号");
		}
		if(record.getWxMoney()==0) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请输入 支付金额");
		}
		JsonResult result=null;
		try {
			//  result = payLogService.insert(record);
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonResult(ResultConst.FALSE_STATE, false, "系统错误");
		}
		return result;
	}

}
