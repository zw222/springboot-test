package com.example.demo.conroller;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.pojo.WdfAdminUser;
import com.example.demo.service.WdfAdminUserService;
import com.example.demo.utils.JsonResult;
import com.example.demo.utils.MD5Utils;
import com.example.demo.utils.ResultConst;

@RestController
@RequestMapping("adminUser")
public class WdfAdminUserController {
	
	@Autowired
	private WdfAdminUserService  wdfAdminUserService;
	/**
	 * 查寻用户
	 * @param name
	 */
	@RequestMapping(value="login")
	@CrossOrigin
	public Object login(String name,String password) {
		System.err.println(name);
		if(StringUtils.isBlank(name)) {
			return  new JsonResult(ResultConst.FALSE_STATE, false, "请输入账号");
		}
		if(StringUtils.isBlank(password)) {
			return  new JsonResult(ResultConst.FALSE_STATE, false, "请输入密码");
		}
		try {
			WdfAdminUser user = wdfAdminUserService.selectByName(name.trim());
			if(user==null) {
				return  new JsonResult(ResultConst.FALSE_STATE, false, "用户不存在");
			}
			String encode = MD5Utils.getMD5Str(password.trim());
			if(user.getPassWord().equals(encode)) {
				return  new JsonResult(ResultConst.SUCCESS_STATE, true, "登录成功",user.getStatue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			return  new JsonResult(ResultConst.FALSE_STATE, false, "系统错误");
		}
		return  new JsonResult(ResultConst.FALSE_STATE, false, "密码错误");
	}
	
	
	//gdyb21LQTcIANtvYMT7QVQ==  
	public static void main(String[] args) {
		String encode=null;
		try {
			encode = MD5Utils.getMD5Str("1234");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println(encode);
	}
	


}
