package com.example.demo.conroller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.pojo.Phone;
import com.example.demo.service.SavePhoneService;
import com.example.demo.utils.JsonResult;
/**
 * 官网首页电话保存
 * @author Hasee
 *
 */
@Controller
@ResponseBody
public class SavePhoneController {
	
	@Autowired
	private SavePhoneService savePhoneService;
	/**
	 * 保存手机号码
	 * @param phoneNumber
	 * @return
	 */
	@RequestMapping("/savephone")
	@CrossOrigin
	public Object Savephone(String phoneNumber){
		
		Integer num =0;
		try {
			 num = savePhoneService.Savephone(phoneNumber);
			 if(num==0) {
				 return new JsonResult("500", false, "保存失败");
			 }
		} catch (Exception e) {
			e.printStackTrace();
			return new JsonResult("500", false, "保存失败");
		}
		return new JsonResult("200", true, "保存成功");
		
	}
	/**
	 * 获取电话列表
	 * @return
	 */
	@RequestMapping("/getPhoneList")
	@ResponseBody
	@CrossOrigin
	public Object getPhoneList(int start, int length,
			Phone phone, HttpServletRequest request) {
		Map<String, Object> map = null;
			
		if (start < 0) {
			start = 0;
		}
		if (length != 15) {
			length = 15;
		}
		try {
			map = savePhoneService.getPhoneList(start, length, phone);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
}
