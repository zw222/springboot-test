package com.example.demo.conroller;
/**
 * 用户咨询
 * @author Hasee
 *
 */

import java.text.ParseException;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.pojo.WdfUserInfo;
import com.example.demo.service.WdfUserInfoService;
import com.example.demo.utils.JsonResult;
import com.example.demo.utils.ResultConst;

/**
 * 咨询信息
 * 
 * @author Hasee
 *
 */
@RequestMapping("userInfo")
@RestController
public class WdfUserInfoController {
	@Autowired
	private WdfUserInfoService WdfUserInfoService;

	/**
	 * 保存咨询信息
	 * 
	 * @param record
	 * @return
	 */
	@RequestMapping("insertUserInfo")
	@CrossOrigin
	public Object insert(WdfUserInfo record) {
		if (record == null) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请输入姓名和手机号");
		}
		if (StringUtils.isBlank(record.getName())) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请输入姓名");
		}
		if (StringUtils.isBlank(record.getPhone())) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "请输入手机号码");
		}
		if (record.getType() ==null) {
			return new JsonResult(ResultConst.FALSE_STATE, false, "非法操作");
		}
		int insert = WdfUserInfoService.insert(record);
		if(insert==1) {
			return new JsonResult(ResultConst.SUCCESS_STATE, true, "操作成功");
		}
		return new JsonResult(ResultConst.FALSE_STATE, false, "操作失败");
	}

	/**
	 * 资讯列表
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param StartTime
	 * @param endTime
	 * @return
	 */
	@RequestMapping("selectUserInfoList")
	@CrossOrigin
	public Object name(Integer pageNumber, Integer pageSize,
			@RequestParam(value = "StartTime", required = false) String StartTime,
			@RequestParam(value = "endTime", required = false) String endTime) {
		HashMap<String, Object> list = null;
		try {
			list = WdfUserInfoService.selectUserInfoList(pageNumber, pageSize, StartTime, endTime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return list;
	}
}
