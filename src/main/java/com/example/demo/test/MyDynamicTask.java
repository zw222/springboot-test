package com.example.demo.test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@RestController
@RequestMapping("/test")
public class MyDynamicTask  {

    private static Logger log = LoggerFactory.getLogger(MyDynamicTask.class);

    @Autowired
    private ScheduledUtil scheduledUtil1;

    @Autowired
    private ScheduledUtil scheduledUtil2;

    @Autowired
    private ScheduledUtil scheduledUtil3;


    @GetMapping
    public void getCron(String time,String name,Integer task) {
    	time="0/3 * * * * ?";
        if(task==1){
        	 name="1111";
            scheduledUtil1.setCron(time);
            scheduledUtil1.setName(name);
        }else if(task==2){
        	name="2222";
            scheduledUtil2.setCron(time);
            scheduledUtil2.setName(name);
        }else if (task==3){
        	 name="3333";
            scheduledUtil3.setCron(time);
            scheduledUtil3.setName(name);
        }
    }
}
