package com.example.demo;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.dao.slaver.WdfPayLogerMapper;
import com.example.demo.pojo.WdfPayLoger;
import com.example.demo.pojo.WdfPayLogerExample;
import com.example.demo.pojo.WdfPayLogerExample.Criteria;
import com.github.pagehelper.PageHelper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	WdfPayLogerMapper wdfPayLogerMapper;

	@Test
	public void contextLoads() {
		WdfPayLoger loger = new WdfPayLoger();
		loger.setCreateTime(new Date());
		loger.setUserName("zhangsan");
		loger.setOrderNumber("1212");
		loger.setState((byte) 1);
		loger.setWxMoney(20);
		wdfPayLogerMapper.insert(loger);
	}

	@Test
	public void testUpdate() {

		WdfPayLoger wdfPayLoger = new WdfPayLoger();
		wdfPayLoger.setOrderNumber("555");
		wdfPayLoger.setUserName("wangwu");
		wdfPayLoger.setWxMoney(100);
		wdfPayLoger.setPayTime(new Date());
		wdfPayLoger.setState((byte) 2);
		WdfPayLogerExample example = new WdfPayLogerExample();

		int num = wdfPayLogerMapper.updateByPrimaryOrderNum(wdfPayLoger);
		System.out.println(num);
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testUpdate2() throws Exception {
		PageHelper.startPage(1, 3);
		WdfPayLogerExample wdfPayLogerExample = new WdfPayLogerExample();
		Criteria criteria = wdfPayLogerExample.createCriteria();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = formatter.parse("2019-03-25");
		Date date2 = formatter.parse("2019-03-27");
		criteria.andCreateTimeBetween(date1, date2);
		//criteria.andOrderNumberEqualTo("123");
		List<WdfPayLoger> selectByExample = wdfPayLogerMapper.selectByExample(wdfPayLogerExample);
		for (WdfPayLoger wdfPayLoger2 : selectByExample) {
			System.err.println(wdfPayLoger2.getOrderNumber());
			System.err.println(selectByExample.size());
		}
		System.out.println(selectByExample);
	}
	
	public static void main(String[] args) {
		LocalDate date = LocalDate.parse("2019-03-05");
		System.out.println(date);
	}
}
